<?php
namespace common\models;

use Yii;

class CatalogProducts extends \yii\db\ActiveRecord
{
    public $path = 'images/catalogproducts/';

    public static function tableName()
    {
        return 'catalog_products';
    }

    public function rules()
    {
        return [
            [['title', 'description', 'name', 'sort', 'status', 'level'], 'required'],
            [['parent_id', 'sort', 'status', 'level'], 'integer'],
            [['description', 'created_at'], 'string'],
            [['title', 'name', 'url', 'img'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'name' => 'Название',
            'url' => 'Url',
            'img' => 'Картинка',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'created_at' => 'Дата создание',
        ];
    }

    public function generateCyrillicToLatin(){
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            ',','.',' ','/','(',')','|','_'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            '-','-','-','-','-','-','-','-'
        ];
        $text = str_replace($cyr,$lat, $this->name);
        if(substr($text, -1) == '-') $text = substr($text, 0, -1);
        return preg_replace('/[^a-zA-Z0-9-]/','', $text);
    }

    public function upload()
    {
        $time = time();
        $this->img->saveAs($this->path. $time . $this->img->baseName . '.' . $this->img->extension);
        return $time . $this->img->baseName . '.' . $this->img->extension;
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = CatalogProducts::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParent()
    {
        return $this->hasOne(CatalogProducts::className(), ['id' => 'parent_id']);
    }

    public function getChilds()
    {
        return $this->hasMany(CatalogProducts::className(), ['parent_id' => 'id']);
    }

    public function getChildstop()
    {
        return $this->hasMany(CatalogProducts::className(), ['parent_id' => 'id'])->limit(24);
    }

    public function getChildsmore()
    {
        return $this->hasMany(CatalogProducts::className(), ['parent_id' => 'id'])->offset(16);
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    public function getProductsactive()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id'])->where('status = 1');
    }
	
	public function getAllProducts()
    {
        $id = $this->id;
        $catalogs = CatalogProducts::find()->where('status = 1')->all();
        $result = $this->get_cat($catalogs);
        $result = $this->get_level3($result, $id);
        return $result;
    }

    public function get_cat($catalogs)
    {
        $arr_cat = array();
        if(count($catalogs)) {
            //В цикле формируем массив
            foreach($catalogs as $v) {
                //Формируем массив, где ключами являются адишники на родительские категории
                if(empty($arr_cat[$v['parent_id']])) {
                    $arr_cat[$v['parent_id']] = [];
                }
                $arr_cat[$v['parent_id']][] = $v;
            }
            //возвращаем массив
            return $arr_cat;
        }
    }

    public function get_level3($result, $id)
    {
        $arr = [];
        foreach ($result as $val1){
            foreach($val1 as $val2){
                if($val2->id == $id) {
                    if (!empty($result[$val2->id]))
                        foreach ($result[$val2->id] as $val3) {
                            if (!empty($result[$val3->id])) {
                                foreach ($result[$val3->id] as $val4) {
                                    $arr[] = $val4->id;
                                }
                            }else{
                                $arr[] = $val3->id;
                            }
                        }
                }
            }
        }

        if($arr)
            $arr = Products::find()->where('category_id in ('.implode(',', $arr).')')->all();

        return $arr;
    }


    public static function getProductsByID($id, $sql="", $quantity="")
    {

        $arr = [];
        $catalog = CatalogProducts::findAll(['status' => 1]);
        $arr[] = $id;
        foreach ($catalog as $v) {
            if ($v->parent_id == $id) {
                $arr[] = $v->id;
                $level2 = CatalogProducts::findAll(['parent_id' => $v->id]);
                if ($level2 != null) {
                    foreach ($level2 as $v2) {
                        if ($v2->parent_id == $v->id) {
                            $arr[] = $v2->id;
                            $level3 = CatalogProducts::findAll(['parent_id' => $v2->id]);
                            if ($level3 != null) {
                                foreach ($level3 as $v3) {
                                    if ($v3->parent_id == $v2->id) {
                                        $arr[] = $v3->id;
                                        $level4 = CatalogProducts::findAll(['parent_id' => $v3->id]);
                                        if ($level4 != null) {
                                            foreach ($level4 as $v4) {
                                                if ($v4->parent_id == $v3->id) {
                                                    $arr[] = $v4->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if($arr) {
            if ($quantity != "") {
                $products = Products::find()->where('category_id in (' . implode(',', $arr) . ') ' . $sql)->offset($quantity)->limit(10)->all();
            }else{
                $products = Products::find()->where('category_id in (' . implode(',', $arr) . ') ' . $sql)->all();
            }
        }

        return $products;

    }

    public static function getFirstLevel(){
        return CatalogProducts::find()->where('status = 1 AND level = 1')->all();
    }

    public static function getSecondLevelByID($catalog_id){
        return CatalogProducts::find()->where('status = 1 AND level = 2 AND parent_id = '.$catalog_id)->all();
    }
}
