<?php
namespace common\models;

use Yii;

class PromoUsers extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'promo_users';
    }

    public function rules()
    {
        return [
            [['user_id', 'promo_id'], 'required'],
            [['user_id', 'promo_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'promo_id' => 'Promo ID',
        ];
    }
}
