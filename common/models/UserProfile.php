<?php
namespace common\models;

use Yii;

class UserProfile extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_profile';
    }

    public function rules()
    {
        return [
            [['fio'], 'required'],
            [['user_id'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['fio'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя ',
            'fio' => 'ФИО',
            'date_of_birth' => 'День рождение',
        ];
    }

    public function fields()
    {
        return [
            'fio'
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date_of_birth, 'long');
    }

    public function getFio(){
        return $this->fio;
    }

    public static function getAll(){
        return UserProfile::find()->all();
    }


}
