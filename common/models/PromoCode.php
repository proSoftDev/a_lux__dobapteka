<?php
namespace common\models;

use Yii;

class PromoCode extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'promo_code';
    }

    public function rules()
    {
        return [
            [['name', 'code', 'percent', 'quantity', 'start_date', 'end_date'], 'required'],
            [['percent', 'quantity'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Промокод',
            'percent' => 'Скидка',
            'quantity' => 'Количества',
            'start_date' => 'Дата начала',
            'end_date' => 'Дата окончания',
        ];
    }
}
