<?php
namespace common\models;

use Yii;

class Analogy extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'analogy';
    }

    public function rules()
    {
        return [
            [['product1', 'product2'], 'required'],
            [['product1', 'product2'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product1' => 'Product1',
            'product2' => 'Product2',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'product2']);
    }


    public static function getProducts($id){

        $product = Analogy::findAll(['product1' => $id]);
        $res = array();
        foreach ($product as $v) $res[$v->id] = $v->id;
        return $res;
    }

    public static function getProductsBySql($Product){
        $analogy = Analogy::find()->where('product1 = '.$Product->id. ' OR product2 = '.$Product->id)->all();
        $in = [];
        $sql = 0;
        foreach ($analogy as $v){
            if($Product->id == $v->product1)
                $in[] = $v->product2;
            else
                $in[] = $v->product1;
        }

        if($in)
            $sql = 'id in ('.implode(',', $in).')';

        return Products::find()->where($sql)->all();
    }




}
