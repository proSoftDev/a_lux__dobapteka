<?php
namespace common\models;

use Yii;

class Products extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/products/';
    public $count;

    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['category_id', 'title', 'description', 'name', 'sort', 'status', 'price', 'created_at',
                'text'], 'required'],
            [['category_id', 'sort', 'status', 'price', 'top_month', 'discount', 'bonus','isHit','isNew'], 'integer'],
            [['description', 'images', 'text'], 'string'],
            [['created_at'], 'safe'],
            [['title', 'name', 'url', 'status_products', 'model', 'country'], 'string', 'max' => 255],
            ['files', 'each', 'rule' => ['image']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'name' => 'Название',
            'url' => 'Url',
            'images' => 'Картинки',
            'top_month' => 'Товар месяца',
            'isHit' => 'Хиты продаж',
            'isNew' => 'Статус',
            'discount' => 'Скидка',
            'sort' => 'Сортировка',
            'status' => 'В наличие',
            'status_products' => 'Статус',
            'created_at' => 'Дата создание',
        ];
    }

    public function upload()
    {
        $time = time();
        $this->images->saveAs($this->path. $time . $this->images->baseName . '.' . $this->images->extension);
        return $time . $this->images->baseName . '.' . $this->images->extension;
    }


    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(Products::find()->all(),'id','name');
    }


    public function generateCyrillicToLatin(){
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
			',','.',' ','/','(',')','|','_'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
           	'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
			'-','-','-','-','-','-','-','-'
        ];
        $text = str_replace($cyr,$lat, $this->name);
		if(substr($text, -1) == '-') $text = substr($text, 0, -1);
        return preg_replace('/[^a-zA-Z0-9-]/','', $text);
    }





    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Products::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
        return $this->hasOne(CatalogProducts::className(), ['id' => 'category_id']);
    }

    public function getThis_country()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    public function getSop_tovary()
    {
        return $this->hasOne(SopTovary::className(), ['product1' => 'id']);
    }


    public static function getSum()
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                if ($v->discount == null) {
                    $price = (int)$v->price;
                } else {
                    $price = (int)$v->price * (100 - $v->discount) / 100;
                }
                $sum += (int)$v->count * $price;
            }
        }

        if(!Yii::$app->user->isGuest){
            if(isset($_SESSION['bonus'])){
                $user = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);
                $bonus = $user->sum;
                if($bonus > $sum){
                    $sum = 0;
                }else{
                    $sum = $sum - $bonus;
                }
            }
        }

        return intval($sum);
    }


    public static function getSumWithoutBonus()
    {
        $sum = 0;
        if ($_SESSION['basket'] != null) {
            foreach ($_SESSION['basket'] as $v) {
                if ($v->discount == null) {
                    $price = (int)$v->price;
                } else {
                    $price = (int)$v->price * (100 - $v->discount) / 100;
                }
                $sum += (int)$v->count * $price;
            }
        }
        return intval($sum);
    }

    public static function getSumProduct($id){
        $sum =0;
        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($v->id == $id){
                    if($v->discount == null){
                        $price = (int)$v->price;
                    }else{
                        $price = (int)$v->price*(100-$v->discount)/100;
                    }
                    $sum+=(int)$v->count*$price;
                }
            }
        }
        return intval($sum);
    }

    public static function getOrderProductsAsArray(){
        $response = array();
        foreach ($_SESSION['basket'] as $v) {
            $response[$v->id] = $v->id;
        }
        return $response;
    }


    public static function getSearchProducts(){
        $products = Products::find()->all();
        $result = "";
        foreach ($products as $v){
            $result .= $v->name.',';
        }
        $result = substr($result,0,-1);
        return $result;
    }

    public function getCalculatePrice(){
         if(!$this->discount){
             $price =  $this->price;
        }else{
             $price = $this->price * (100 - $this->discount) / 100;
        }

         return intval($price);
    }

    public static function getSimilarProducts($text){
        return $product = Products::find()->where("name LIKE '%$text%'")->all();
    }


    public function getFavoriteStatus(){
        if(UserFavorites::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $this->id])) return 1;
        else return 0;
    }


}
