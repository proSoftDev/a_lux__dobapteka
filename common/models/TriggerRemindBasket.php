<?php
namespace common\models;

use Yii;

class TriggerRemindBasket extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'trigger_remind_basket';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Текст',
        ];
    }

    public static function getContent(){
        return TriggerRemindBasket::find()->one();
    }

}
