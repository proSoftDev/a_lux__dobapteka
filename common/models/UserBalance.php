<?php
namespace common\models;

use frontend\controllers\CardController;
use Yii;

class UserBalance extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_balance';
    }

    public function rules()
    {
        return [
            [['user_id', 'sum'], 'required'],
            [['user_id', 'sum'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sum' => 'Sum',
        ];
    }


    public static function createUserBalanceForRegistration($user_id){
        $balance = new UserBalance();
        $balance->user_id = $user_id;
        $balance->sum = 500;
        return $balance->save();
    }


    public static function addBonus($bonus){
        $sum = CardController::getSumBasketWithoutDelivery();
        $userBonus = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);
        $userBonus->sum += $sum*(3/100)+$bonus;
        $userBonus->save(false);
    }
}
