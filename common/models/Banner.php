<?php
namespace common\models;

use Yii;
/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $image
 * @property string $text
 */

class Banner extends \yii\db\ActiveRecord
{

    public $path = 'images/banner/';

    public static function tableName()
    {
        return 'banner';
    }

    public function rules()
    {
        return [

            [['text'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }



    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'image' => 'Картинка',
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public function getImageUrl(){
        return '@backend/web/'.$this->path;
    }

    public static function getAll(){
        return Banner::find()->all();
    }
}
