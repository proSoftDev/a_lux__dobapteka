<?php
namespace common\models;

use Yii;

class Admission extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'admission';
    }

    public function rules()
    {
        return [
            [['user_id', 'menu', 'mainsub', 'banner', 'advantages', 'filial', 'catalog', 'products', 'country', 'geocoords', 'promocode', 'gift', 'discount', 'free_delivery', 'trigger_birthday', 'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'orders', 'contact', 'logo', 'text', 'review'], 'required'],
            [['user_id', 'menu', 'mainsub', 'banner', 'advantages', 'filial', 'catalog', 'products', 'country', 'geocoords', 'promocode', 'gift', 'discount', 'free_delivery', 'trigger_birthday', 'trigger_holiday', 'trigger_tovar_month', 'trigger_remind_basket', 'orders', 'contact', 'logo', 'text', 'review'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'menu' => 'Menu',
            'mainsub' => 'Mainsub',
            'banner' => 'Banner',
            'advantages' => 'Advantages',
            'filial' => 'Filial',
            'catalog' => 'Catalog',
            'products' => 'Products',
            'country' => 'Country',
            'geocoords' => 'Geocoords',
            'promocode' => 'Promocode',
            'gift' => 'Gift',
            'discount' => 'Discount',
            'free_delivery' => 'Free Delivery',
            'trigger_birthday' => 'Trigger Birthday',
            'trigger_holiday' => 'Trigger Holiday',
            'trigger_tovar_month' => 'Trigger Tovar Month',
            'trigger_remind_basket' => 'Trigger Remind Basket',
            'orders' => 'Orders',
            'contact' => 'Contact',
            'logo' => 'Logo',
            'text' => 'Текст',
            'review' => 'Review',
        ];
    }
}
