<?php
namespace common\models;

use DateTime;
use Yii;

class Filial extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'filial';
    }

    public function rules()
    {
        return [
            [['address', 'from_time', 'to_time', 'telephone', 'longitude', 'latitude', 'city_id'], 'required'],
            [['address'], 'string'],
            [['from_time', 'to_time'], 'safe'],
            [['city_id'], 'integer'],
            [['telephone', 'longitude', 'latitude'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'from_time' => 'От',
            'to_time' => 'До',
            'telephone' => 'Телефон',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'city_id' => 'Город',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Filial::find()->all(),'id','name');
    }

    public function getCurrentStatus(){
        date_default_timezone_set('Asia/Almaty');

        try {
            $from_time = new DateTime($this->from_time);
            $to_time = new DateTime($this->to_time);
        } catch (\Exception $e) {
        }


        $date = date('H', time())*3600+date('i', time())*60;
        if($from_time->format('H') < $to_time->format('H')) {
            $from_time = $from_time->format('H') * 3600 + $from_time->format('i') * 60;
            $to_time = $to_time->format('H') * 3600 + $to_time->format('i') * 60;
        }else{
            $from_time = $from_time->format('H') * 3600 + $from_time->format('i') * 60;
            $to_time = $to_time->format('H') * 3600 + $to_time->format('i') * 60+24*3600;
        }

        if($date > $from_time && $date < $to_time){
            return "Открыто";
        }else{
            return "Закрыто";
        }
    }

    public static function getAll(){
        return Filial::findAll(['city_id'=>Yii::$app->session["city_id"]]);
    }

}
