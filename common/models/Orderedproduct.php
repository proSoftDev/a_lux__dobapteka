<?php
namespace common\models;

use Yii;
use common\models\Products;

class Orderedproduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderedproduct';
    }

    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count','isGift'], 'required'],
            [['order_id', 'product_id', 'count','isGift'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }
	
	public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
