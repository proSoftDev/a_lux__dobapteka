<?php
namespace common\models;

use Yii;

class Text extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'text';
    }

    public function rules()
    {
        return [
            [['text', 'type_id'], 'required'],
            [['text'], 'string'],
            [['type_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'type_id' => 'Тип',
        ];
    }

    public function getType()
    {
        return $this->hasOne(TextType::className(), ['id' => 'type_id']);
    }

    public static  function getSMS(){
        $model = Text::findOne(["type_id" => 4]);
        return $model->text;
    }
}
