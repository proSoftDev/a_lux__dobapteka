<?php
namespace common\models;

use Yii;

class UserAddedProduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_added_product';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'date' => 'Date',
        ];
    }

    public static function addProduct($user_id, $product_id){
        $model = new UserAddedProduct();
        $model->user_id = $user_id;
        $model->product_id = $product_id;
        $model->save(false);
    }

    public static function getAll(){
        return UserAddedProduct::find()->all();
    }

}
