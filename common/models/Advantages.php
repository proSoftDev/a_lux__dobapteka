<?php
namespace common\models;

use Yii;

class Advantages extends \yii\db\ActiveRecord
{

    public $path = 'images/advantages/';

    public static function tableName()
    {
        return 'advantages';
    }

    public function rules()
    {
        return [
            [['name', 'text', 'url'], 'required'],
            [['text','content'], 'string'],
            [['name','url'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'text' => 'Текст',
            'content' => 'Содержание',
            'url' => 'Ссылка'
        ];
    }

    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public static function getAll(){
        return Advantages::find()->all();
    }


}
