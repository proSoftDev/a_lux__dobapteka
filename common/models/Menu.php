<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $text
 * @property int $status
 * @property int $isNew
 * @property int $content
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'status', 'metaName', 'metaDesc', 'metaKey','url'], 'required'],
            [['status','isNew'], 'integer'],
            [['metaDesc', 'metaKey','content'], 'string'],
            [['text', 'metaName','url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => '	Заголовок',
            'status' => 'Статус',
            'content' => 'Содержание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'url' => 'Ссылка'
        ];
    }

}
