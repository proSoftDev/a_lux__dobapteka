<?php
namespace common\models;

use Yii;

class Review extends \yii\db\ActiveRecord
{
    const isRead = 1;
    const isNotRead  = 0;

    public static function tableName()
    {
        return 'review';
    }

    public function rules()
    {
        return [
            [['name', 'surname', 'city', 'telephone', 'email'], 'required'],
            [['comment'], 'string'],
            [['isRead'], 'integer'],
            [['name', 'surname', 'email', 'telephone', 'city'], 'string', 'max' => 255],
            [['email'],'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'E-mail',
            'telephone' => 'Телефон',
            'city' => 'Город',
            'comment' => 'Сообщение',
            'isRead' => 'Статус',
            'created_at' => 'Дата создание'
        ];
    }

    public function isReaded(){
        $this->isRead = self::isRead;
        return $this->save(false);
    }
}
