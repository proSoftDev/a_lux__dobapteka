<?php
namespace common\models;

use Yii;

class TriggerTovarMonth extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'trigger_tovar_month';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Текст',
        ];
    }

    public static function getContent(){
        return TriggerTovarMonth::find()->one();
    }
}
