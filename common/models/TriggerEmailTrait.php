<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.08.2019
 * Time: 10:17
 */

namespace common\models;

use DateTime;
use Yii;

trait TriggerEmailTrait
{


    public function actionUpdatedTovarMonth()
    {
        $top_month = TriggerTovarMonth::getContent();
        $user_emails = User::findAll(['role'=>null]);
        $message = $top_month->content;

        foreach ($user_emails as $v){
            Yii::$app->mailer
                ->compose('tovar_month',compact('message','host'))
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->email)
                ->setSubject("")
                ->send();
        }
    }

    public function actionRemindBasket()
    {
        date_default_timezone_set('Asia/Almaty');
        $remind_basket = TriggerRemindBasket::getContent();
        $message = $remind_basket->content;
        $host = Yii::$app->request->hostInfo;;
        $added_product = UserAddedProduct::getAll();
        $check = [];
        foreach ($added_product as $v){
            try {
                $date = new DateTime($v->date);
                $now = new DateTime();
            }catch (\Exception $e) {}

            $interval = $now->diff($date);
            if($interval->d == 7){
                if($check[$v->user_id] != $v->user_id){
                    $user = User::findOne($v->user_id);
                    $mail = Yii::$app->mailer
                        ->compose('remind_basket',compact('message','host'))
                        ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                        ->setTo($user->email)
                        ->setSubject("")
                        ->send();
                    if($mail){
                        $check[$v->user_id] = $v->user_id;
                        UserAddedProduct::deleteAll(['user_id'=>$v->user_id]);
                    }
                }
            }
        }
    }


    public function actionBirthday()
    {
        date_default_timezone_set('Asia/Almaty');
        $birthdayTrigger = TriggerBirthday::getContent();
        $message = $birthdayTrigger->content;
        $user_profile = UserProfile::getAll();
        $birthday = array();
        $host = Yii::$app->request->hostInfo;
        foreach ($user_profile as $user){
            $time = strtotime($user->date_of_birth);
            if(date('m-d') == date('m-d', $time)) array_push($birthday,$user);
        }

        $sendedMessage = UserSendedMessage::getAllBirthdaySendMessages();
        foreach ($birthday as $v){
            $status = true;
            foreach ($sendedMessage as $val){
                if($v->user_id == $val->user_id){
                    if(date('y') == $val->year) {
                        $status = false;
                        break;
                    }
                }
            }
            if($status){
                $user = User::findOne($v->user_id);
                $name = $v->name;
                $mail = Yii::$app->mailer
                    ->compose('birthday',compact('message','host','name'))
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($user->email)
                    ->setSubject("С Днем рождения")
                    ->send();

                if($mail){
                    $sendedMessage = new UserSendedMessage();
                    $sendedMessage->saveBirthdayMessage($v->user_id,date('y'),date('m'),date('d'));

                }
            }

        }
    }


    public function actionHoliday()
    {
        date_default_timezone_set('Asia/Almaty');
        $sendedMessage = UserSendedMessage::getAllHolidaySendMessages();
        $user_emails = User::findAll(['role'=>null]);
        $host = Yii::$app->request->hostInfo;
        $holidayStatus = false;
        $holidayTrigger = TriggerHoliday::getAll();
        foreach ($holidayTrigger as $v){
            $time = strtotime($v->date);
            if(date('m-d') == date('m-d', $time)) {
                $holidayStatus = true;
                $message = $v->content;
                break;
            }
        }

        if($holidayStatus) {
            foreach ($user_emails as $v) {
                $status = true;
                foreach ($sendedMessage as $val) {
                    if ($v->id == $val->user_id) {
                        if (date('y') == $val->year && date('m') == $val->month && date('d') == $val->day) {
                            $status = false;
                            break;
                        }
                    }
                }
                if ($status) {
                    $userProfile = UserProfile::findOne(['user_id' => $v->id]);
                    $name = $userProfile->name;
                    $mail = Yii::$app->mailer
                        ->compose('holiday', compact('message', 'host', 'name'))
                        ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                        ->setTo($v->email)
                        ->setSubject("С праздником")
                        ->send();

                    if ($mail) {
                        $sendedMessage = new UserSendedMessage();
                        $sendedMessage->saveHolidayMessage($v->id,date('y'),date('m'),date('d'));
                    }
                }
            }
        }
    }

}
