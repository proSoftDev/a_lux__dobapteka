<?php
namespace common\models;

use Yii;

class SopTovary extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sop_tovary';
    }

    public function rules()
    {
        return [
            [['product1', 'product2'], 'required'],
            [['product1', 'product2'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product1' => 'Product1',
            'product2' => 'Product2',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'product2']);
    }

    public static function getProducts($id){

        $product = SopTovary::findAll(['product1' => $id]);
        $res = array();
        foreach ($product as $v) $res[$v->id] = $v->id;
        return $res;
    }

    public static function getProductsBySql($Product){
        $sop_tovary = SopTovary::findAll(['product1' => $Product->id]);
        $in = [];
        $sql = 0;
        foreach ($sop_tovary as $v){$in[] = $v->product2;}
        if($in) $sql = 'id in ('.implode(',', $in).')';
        return Products::find()->where($sql)->all();
    }


}
