<?php
namespace common\models;

use Yii;

class Contact extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'contact';
    }

    public function rules()
    {
        return [
            [['telephone', 'address', 'instagram', 'facebook'], 'required'],
            [['address'], 'string'],
            [['telephone', 'instagram', 'facebook'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephone' => 'Телефон',
            'address' => 'Адрес',
            'instagram' => 'Ссылка на Instagram',
            'facebook' => 'Ссылка на Facebook',
        ];
    }
}
