<?php
namespace common\models;

use Yii;

class Catalog extends \yii\db\ActiveRecord
{

    public $path = 'images/catalog/';

    public static function tableName()
    {
        return 'catalog';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

}
