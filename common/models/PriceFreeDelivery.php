<?php
namespace common\models;

use Yii;

class PriceFreeDelivery extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'price_free_delivery';
    }

    public function rules()
    {
        return [
            [['price'], 'required'],
            [['price'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Цена от',
        ];
    }

    public static function getContent(){
        return PriceFreeDelivery::find()->one();
    }
}
