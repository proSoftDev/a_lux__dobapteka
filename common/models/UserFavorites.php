<?php
namespace common\models;

use Yii;

class UserFavorites extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_favorites';
    }

    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
        ];
    }

    public static function getByUser(){
        return UserFavorites::findAll(['user_id' => Yii::$app->user->id]);
    }

    public static function findByUserAndProduct($user_id, $product_id){
        return UserFavorites::find()->where('user_id='.$user_id.' AND product_id='.$product_id)->one();
    }

    public function saveFavorite($user_id, $product_id){
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        return $this->save(false);
    }
}
