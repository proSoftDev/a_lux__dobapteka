<?php
return [
    'adminEmail' => 'robot@chat-bots.kz',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
