<?php
return [
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'chat-bots.kz',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'robot@chat-bots.kz',
                'password' => '1q2w3e4r',
            ],
            'useFileTransport' => false,
        ],
        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
//            'language' => 'ru-RU',
            'class' => 'yii\i18n\Formatter',
        ],

        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],

    ],
];
