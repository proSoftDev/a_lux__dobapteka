<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;
use common\models\Products;
use common\models\CatalogProducts;

class ImportController extends Controller
{
    public function actionRun() {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        if (($handle = fopen("./dobapteka.chat-bots.kz/standart_n/warebase.csv", "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                if(!$count) {$count++;continue;}
                if(empty($data[14]) || empty($data[15])) {continue;}

                $data = array_map(function($str) {
                    return iconv( "Windows-1251", "UTF-8", $str );
                }, $data );

                if(!($catalog = CatalogProducts::find()->where(['title' => $data[14], 'level' => 1])->one())) {
                    $catalog = new CatalogProducts();
                    $catalog->attributes = [
                        'title' => $data[14],
                        'name' => $data[14],
                        'description' => $data[14],
                        'level' => 1,
                        'status' => 1,
                    ];
                    $catalog->url = $catalog->generateCyrillicToLatin();
                    $catalog->save(false);
                }

                if(!($catalog_child = CatalogProducts::find()->where(['title' => $data[15], 'level' => 2])->one())) {
                    $catalog_child = new CatalogProducts();
                    $catalog_child->attributes = [
                        'title' => $data[15],
                        'name' => $data[15],
                        'description' => $data[15],
                        'parent_id' => $catalog->id,
                        'level' => 2,
                        'status' => 1,
                    ];
                    $catalog_child->url = $catalog_child->generateCyrillicToLatin();
                    $catalog_child->save(false);
                }

                if(!($model = Products::find()->where(['title' => $data[2]])->one())) {
                    $model = new Products();
                    $model_data = [
                        'category_id' => $catalog_child->id,
                        'title' => !empty($data[2]) ? $data[2] : '',
                        'description' => !empty($data[12]) ? $data[12] : '',
                        'name' => !empty($data[2]) ? $data[2] : '',
                        'status' => !((int)$data[7]<=0),
                        'price' => !empty($data[13]) ? (int)$data[13] : 0,
                        'text' => !empty($data[12]) ? $data[12] : '',
                        'country' => '',
                        'model' => '',
                        'isHit' => 0,
                        'isNew' => 0
                    ];
                }else{
                    $model_data = [
                        'category_id' => $catalog_child->id,
                        'title' => !empty($data[2]) ? $data[2] : '',
                        'description' => !empty($data[12]) ? $data[12] : '',
                        'name' => !empty($data[2]) ? $data[2] : '',
                        'status' => !((int)$data[7]<=0),
                        'price' => !empty($data[13]) ? (int)$data[13] : 0,
                        'text' => !empty($data[12]) ? $data[12] : '',
                        'country' => '',
                        'model' => '',
                        'isHit' => $model->isHit,
                        'isNew' => $model->isNew
                    ];
                }

                $model->attributes = $model_data;
                $model->url = $model->generateCyrillicToLatin();
                $model->save(false);
                $model->url = $model->url.'_'.$model->id;
                $model->save(false);

                curl_setopt($curl, CURLOPT_URL, 'https://biosfera.kz/product/search?search='.$model->name);
                $page = curl_exec($curl);

                $pokemon_doc = new \DOMDocument;
                libxml_use_internal_errors(true);
                $pokemon_doc->loadHTML($page);
                libxml_clear_errors();

                $pokemon_xpath = new \DOMXPath($pokemon_doc);

                $image_url = $pokemon_xpath->evaluate('string(/html/body/div[@class="contentPart"]/div[@class="lotList"]/div[@class="lotItem"]/div[@class="lotImage"]/a/img/@src)');
                $image_url = str_replace('188x188', '320x320', $image_url);
                $file_name = basename($image_url);

                if(file_put_contents(Yii::$app->basePath.'/../backend/web/images/products/'.$file_name, file_get_contents($image_url))) {
                    $model->images = serialize([$file_name]);
                    $model->save(false);
                }
                else {}
            }
            fclose($handle);
        }

        curl_close($curl);
    }
}
