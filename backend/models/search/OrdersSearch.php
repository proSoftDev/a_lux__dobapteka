<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

class OrdersSearch extends Orders
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'statusPay','statusProgress', 'sum', 'typePay', 'typeDelivery'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'statusPay' => $this->statusPay,
            'statusProgress' => $this->statusProgress,
            'sum' => $this->sum,
            'typeDelivery' => $this->typeDelivery,
            'typePay' => $this->typePay,
            'created' => $this->created,
        ]);
        $query->orderBy('id DESC');

        return $dataProvider;
    }
}
