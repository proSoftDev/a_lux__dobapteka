<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

class ProductsSearch extends Products
{
    public function rules()
    {
        return [
            [['id', 'category_id', 'sort', 'status','isHit','isNew'], 'integer'],
            [[ 'description', 'name', 'url', 'images', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sort' => $this->sort,
            'status' => $this->status,
            'isHit' => $this->isHit,
            'isNew' => $this->isNew,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'images', $this->images]);

        $query->orderBy('sort ASC');

        return $dataProvider;
    }
}
