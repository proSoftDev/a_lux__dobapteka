<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'User Balances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-balance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать User Balance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'sum',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
