<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Admissions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать Admission', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'page_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
