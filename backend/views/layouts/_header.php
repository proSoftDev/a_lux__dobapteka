<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <? if(Yii::$app->view->params['admission']->review):?>
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Заявки">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-success"><?=count(Yii::$app->view->params['feedback']);?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">У вас <?=count(Yii::$app->view->params['feedback']);?> непрочитанных заявки</li>
                        <li>
                            <ul class="menu">

                                <? if(Yii::$app->view->params['feedback'] != null):?>
                                    <? foreach (Yii::$app->view->params['feedback'] as $v):?>
                                        <li>
                                            <a href="/admin/review/view?id=<?=$v->id;?>">
                                                <i class="fa fa-user text-green"></i> <?=$v->name.' '.$v->surname;?>
                                            </a>
                                        </li>
                                    <? endforeach;?>
                                <? endif;?>
                            </ul>
                        </li>
                        <li class="footer"><a href="/admin/review/">Посмотреть все заявки</a></li>
                    </ul>
                </li>
                <? endif;?>

                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                        <!-- The user image in the navbar-->
                        <img src="/backend/web/user2-160x160.jpg" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?=Yii::$app->user->identity->username;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="/backend/web/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                <?=Yii::$app->user->identity->username;?>
                                <small>2017-2019</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin/admin-profile" class="btn btn-default btn-flat">Профиль</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выйти',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
