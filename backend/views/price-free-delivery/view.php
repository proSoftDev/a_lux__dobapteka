<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Бесплатная доставка';
$this->params['breadcrumbs'][] = ['label' => 'Price Free Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-free-delivery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'raw',
                'attribute' => 'price',
                'value' => function($data){
                    return $data->price .' тг.';
                }
            ],
        ],
    ]) ?>

</div>
