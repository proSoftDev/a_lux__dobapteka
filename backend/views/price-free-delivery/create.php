<?php

use yii\helpers\Html;

$this->title = 'Создание Price Free Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Price Free Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-free-delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
