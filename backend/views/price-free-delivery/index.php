<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Бесплатная доставка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-free-delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'attribute' => 'price',
                'value' => function($data){
                    return $data->price .' тг.';
                }
            ],

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
