<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
            if($model->isRead == 0){
                return ['class' => 'success'];
            }else{
                return ['style' => 'background-color:#fff'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'surname',
                'value' => $model->surname,
                'format' => 'raw',
            ],



            'created_at',


                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
