<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Заказ: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <? if($model->typeDelivery == 1) {
        $address =  ['attribute' => 'address', 'value' => function($data){return $data->address;}];
        $statusProgress =  ['attribute' => 'statusProgress','value' => function ($model) {return backend\controllers\LabelProgress::statusLabel($model->statusProgress);},'format' => 'raw'];
    }else {
        $address = ['attribute' => 'Адрес забора', 'value' => function ($data) {return $data->address;}];
        $statusProgress =  ['attribute' => 'Статус забора','value' => function ($model) {return backend\controllers\LabelDelivery::statusLabel($model->statusProgress);},'format' => 'raw'];
    }?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'id',
            [
                'attribute'=>'Заказчик',
                'value'=>function($model) {
                    if ($model->user_id) return 'Зарегистрирован';
                    else return 'Не зарегистрирован';
                }
            ],
            [
                'attribute'=>'user_id',
                'value'=>function($model) {
                    if ($model->user_id) return $model->userProfile->fio;
                    else return $model->fio;
                }
            ],
            [
                'attribute'=>'Телефон',
                'value'=>function($model){
                    if ($model->user_id) return $model->user->username;
                    else return $model->telephone;
                }
            ],

            [
                'attribute'=> "Email",
                'value'=>function($model){
                    if ($model->user_id) return $model->user->email;
                     else return $model->email;
                }
            ],


            [
                'attribute' => 'typePay',
                'filter' => \backend\controllers\LabelType::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelType::statusLabel($model->typePay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'typeDelivery',
                'filter' => \backend\controllers\LabelDeliveryMethod::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelDeliveryMethod::statusLabel($model->typeDelivery);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \backend\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],


            $statusProgress,


            [
                'format' => 'raw',
                'attribute' => 'change',
                'value' => function($data){
                    return $data->change.' тг';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_promo',
                'value' => function($data){
                    return $data->used_promo.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_elderly_discount',
                'value' => function($data){
                    return $data->used_elderly_discount	.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_bonus',
                'value' => function($data){
                    return $data->used_bonus.' тг';
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'deliverySum',
                'value' => function($data){
                    return $data->deliverySum.' тг';
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'sum',
                'value' => function($data){
                    return $data->sum.' тг';
                }
            ],


            $address,
            [
                'format' => 'raw',
                'attribute' => 'time',
                'value' => function($data){
                    if($data->time != null){
                        return $data->time.' минут';
                    }else{
                        return '(не задано)';
                    }

                }
            ],
            'delivery_id',
            'message_id',
            [
                'attribute' => 'created',
                'value' => function ($model) {
                    return $model->created;
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>



    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                <? if($products != null):?>
                    <? foreach ($products as $v):?>
                        <?$img = unserialize($v->product->images);$img = $img[0]?>
                        <tr>
                            <td><a href="/admin/products/view?id=<?=$v->product->id;?>"><img src="/backend/web/<?=$v->product->path.$img?>" width="50">  <?=$v->product->name;?></a></td>
                            <td><?=$v->count;?></td>
                            <td><?=$v->product->calculatePrice*$v->count;?></td>
                        </tr>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
    </div>


    <? if($gift != null):?>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Подарки:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($gift as $v):?>
                    <?$img = unserialize($v->product->images);$img = $img[0]?>
                    <tr>
                        <td><a href="/admin/products/view?id=<?=$v->product->id;?>"><img src="/backend/web/<?=$v->product->path.$img?>" width="50">  <?=$v->product->name;?></a></td>
                        <td><?=$v->count;?></td>
                    </tr>
                <? endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <? endif;?>



</div>


