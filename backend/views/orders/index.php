<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <? $type = [1=>'По курьеру', 2=>'По банковская карта'];?>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Экспорт в Excel', ['export-file'], ['class' => 'btn btn-success']) ?>
    </p>
    <?echo date_default_timezone_get();;?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'user_id',
                'value'=>function($model) {
                    if ($model->user_id) return $model->userProfile->fio;
                    else return $model->fio;
                }
            ],

			[
                'attribute' => 'typePay',
                'filter' => \backend\controllers\LabelType::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelType::statusLabel($model->typePay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'typeDelivery',
                'filter' => \backend\controllers\LabelDeliveryMethod::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelDeliveryMethod::statusLabel($model->typeDelivery);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \backend\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'created',
                'value' => function ($model) {
                    return $model->created;
                },
                'format' => 'raw',
            ],



            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
