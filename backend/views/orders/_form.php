<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>


    <? if($model->typeDelivery == 1):?>
        <?= $form->field($model, 'statusProgress')->dropDownList(\backend\controllers\LabelProgress::statusList()) ?>
    <? else:?>
        <?= $form->field($model, 'statusProgress')->dropDownList(\backend\controllers\LabelDelivery::statusList()) ?>
    <? endif;?>

    <?= $form->field($model, 'statusPay')->dropDownList([0 => 'Не оплачено', 1 => 'Оплачено']) ?>

    <?= $form->field($model, 'typePay')->dropDownList([ 1 => 'По факту', 2 => 'Онлайн оплата']) ?>

    <?= $form->field($model, 'typeDelivery')->dropDownList([ 1 => 'Курьером', 2 => 'Самовызов']) ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'used_bonus')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_promo')->textInput(['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'used_elderly_discount')->textInput(['disabled'=>'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<div>
    <h2>Продукты заказа:</h2>
    <br>
    <div id="orderedProducts" style="height: 15rem;margin-bottom: 50px;">
        <?php foreach ($products as $v) :?>
            <div style="float: left;margin-right: 25px;">
                <p style="width: 200px"><?=$v->name;?><b>  X <?=$v->count?></b></p>
                <?$img = unserialize($v->images);$img = $img[0]?>
                <a href="/product/<?=$v->url?>">
                    <img src="/backend/web/<?=$v->path.$img?>"  width="100">
                    <p style="margin-top: 10px;"><?=$v->calculatePrice;?><span> тг.</span></p>
                </a>
            </div>
        <?php endforeach;?>
    </div>


    <? if($model->typeDelivery == 2):?>
    <div class="form-group">
        <h2>Добавить продукты</h2>
        <select class="form-control select2" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="OrderedProduct[product][]">
            <? foreach ($model->getAllProducts() as $v):?>
                <option value="<?=$v->id;?>"><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>

    <button type="button" class="btn btn-success" id="addProduct">Добавить продукты</button>
    <? endif;?>
</div>





<script src="/js/jquery-3.2.1.min.js"></script>
<script>
    $('body').on('click', '#addProduct', function () {
        var products = $('select[name="OrderedProduct[product][]"]').val();
        if(products == ''){
            alert('Выберите продукта.');
        }else {
            var res = '(';
            for(var i=0;i<products.length;i++){
                res += products[i]+',';
            }
            res = res.substring(0, res.length - 1);
            res += ')';
            products = res;
            $.ajax({
                url: "/admin/orders/add-product",
                type: "GET",
                data: {products:products,products_id:<?=$model->id;?>},
                success: function(data){
                    $('#orderedProducts').html(data);
                },
                error: function () {
                    alert('Что-то пошло не так.');
                }
            });
        }

    });
</script>
