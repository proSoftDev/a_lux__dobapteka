<?php

use budyaga\cropper\Widget;
use developit\jcrop\Jcrop;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\imagine\Image;



?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

        <?=$form->field($model, 'image')->widget(Widget::className(), [
            'uploadUrl' => Url::toRoute('/banner/uploadPhoto'),
            'width' => 423.5,
            'height' => 210
        ]); ?>

        <div class="form-group">
            <?= Html::a('Сохранить',['save-image', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
