<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Администратор', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->getCreatedDate();
                },
                'format' => 'raw',
            ],


        ],
    ]) ?>



    <h3 >Доступы: </h3></br>
    <div class="form-group field-admission-menu">
        <h4> <span class="label <?=$admission->menu == 1 ? "label-success" : "label-default";?>">Страницы</span></h4>
    </div>


    <label class="control-label" for="user-username">Главная</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-mainsub">
            <h4> <span class="label <?=$admission->mainsub == 1 ? "label-success" : "label-default";?>"> - Заголовок страницы</span></h4>
        </div>


        <div class="form-group field-admission-banner">
            <h4> <span class="label <?=$admission->banner == 1 ? "label-success" : "label-default";?>"> - Баннер</span></h4>
        </div>

        <div class="form-group field-admission-advantages">
            <h4> <span class="label <?=$admission->advantages == 1 ? "label-success" : "label-default";?>"> - Наши преимущества<</span></h4>
        </div>
    </div>


    <label class="control-label" for="user-username">Продукция</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-filial">
            <h4> <span class="label <?=$admission->filial == 1 ? "label-success" : "label-default";?>"> - Филиалы</span></h4>
        </div>

        <div class="form-group field-admission-catalog">
            <h4> <span class="label <?=$admission->catalog == 1 ? "label-success" : "label-default";?>"> - Категории продукции</span></h4>
        </div>

        <div class="form-group field-admission-products">
            <h4> <span class="label <?=$admission->products == 1 ? "label-success" : "label-default";?>"> - Продукция</span></h4>
        </div>

        <div class="form-group field-admission-country">
            <h4> <span class="label <?=$admission->country == 1 ? "label-success" : "label-default";?>"> - Страны</span></h4>
        </div>

        <div class="form-group field-admission-geocoords">
            <h4> <span class="label <?=$admission->geocoords == 1 ? "label-success" : "label-default";?>"> - Координаты</span></h4>
        </div>



        <div class="form-group field-admission-promocode">
            <h4> <span class="label <?=$admission->promocode == 1 ? "label-success" : "label-default";?>"> - Промокоды</span></h4>
        </div>

        <div class="form-group field-admission-gift">
            <h4> <span class="label <?=$admission->gift == 1 ? "label-success" : "label-default";?>"> - Подарки</span></h4>
        </div>


        <div class="form-group field-admission-discount">
            <h4> <span class="label <?=$admission->discount == 1 ? "label-success" : "label-default";?>"> -  Скидка для пенсионеров</span></h4>
        </div>


        <div class="form-group field-admission-free_delivery">
            <h4> <span class="label <?=$admission->free_delivery == 1 ? "label-success" : "label-default";?>"> -  Бесплатная доставка</span></h4>
        </div>
    </div>



    <label class="control-label" for="user-username">Триггерные письма</label>
    <div style="margin-left: 20px;">
        <div class="form-group field-admission-trigger_birthday">
            <h4> <span class="label <?=$admission->trigger_birthday == 1 ? "label-success" : "label-default";?>"> - В дни рождение</span></h4>
        </div>


        <div class="form-group field-admission-trigger_holiday">
            <h4> <span class="label <?=$admission->trigger_holiday == 1 ? "label-success" : "label-default";?>"> -  В празднике</span></h4>
        </div>

        <div class="form-group field-admission-trigger_tovar_month">
            <h4> <span class="label <?=$admission->trigger_tovar_month == 1 ? "label-success" : "label-default";?>"> - При обновлении Товар Месяца</span></h4>
        </div>

        <div class="form-group field-admission-trigger_remind_basket">
            <h4> <span class="label <?=$admission->trigger_remind_basket == 1 ? "label-success" : "label-default";?>"> -  При не осущестивление покупку</span></h4>
        </div>
    </div>

    <div class="form-group field-admission-orders">
        <h4> <span class="label <?=$admission->orders == 1 ? "label-success" : "label-default";?>"> Заказы</span></h4>
    </div>

    <div class="form-group field-admission-contact">
        <h4> <span class="label <?=$admission->contact == 1 ? "label-success" : "label-default";?>"> Контакты</span></h4>
    </div>


    <div class="form-group field-admission-logo">
        <h4> <span class="label <?=$admission->logo == 1 ? "label-success" : "label-default";?>">  Логотип и Копирайт</span></h4>
    </div>


    <div class="form-group field-admission-text">
        <h4> <span class="label <?=$admission->text == 1 ? "label-success" : "label-default";?>"> Текст по сайту</span></h4>
    </div>


    <div class="form-group field-admission-review">
        <h4> <span class="label <?=$admission->review == 1 ? "label-success" : "label-default";?>"> Заявки</span></h4>
    </div>

</div>
