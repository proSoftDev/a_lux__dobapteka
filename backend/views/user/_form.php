<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="user-form" style="padding-bottom: 1200px;">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Доступы</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">

                <div class="form-group field-admission-menu">
                    <input type="hidden" name="Admission[menu]" value="0">
                    <label><input type="checkbox" id="user-menu" name="Admission[menu]" value="1" <?=$admission->menu == 1 ? "checked":"";?>> Страницы</label>
                    <div class="help-block"></div>
                </div>


                <label class="control-label" for="user-username">Главная</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-mainsub">
                        <input type="hidden" name="Admission[mainsub]" value="0">
                        <label><input type="checkbox" id="user-mainsub" name="Admission[mainsub]" value="1" <?=$admission->mainsub == 1 ? "checked":"";?> > - Заголовок страницы</label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-banner">
                        <input type="hidden" name="Admission[banner]" value="0">
                        <label><input type="checkbox" id="user-banner" name="Admission[banner]" value="1" <?=$admission->banner == 1 ? "checked":"";?> > - Баннер</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-advantages">
                        <input type="hidden" name="Admission[advantages]" value="0">
                        <label><input type="checkbox" id="user-advantages" name="Admission[advantages]" value="1" <?=$admission->advantages == 1 ? "checked":"";?> > - Наши преимущества</label>
                        <div class="help-block"></div>
                    </div>
                </div>


                <label class="control-label" for="user-username">Продукция</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-filial">
                        <input type="hidden" name="Admission[filial]" value="0">
                        <label><input type="checkbox" id="user-filial" name="Admission[filial]" value="1" <?=$admission->filial == 1 ? "checked":"";?> > - Филиалы</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-catalog">
                        <input type="hidden" name="Admission[catalog]" value="0">
                        <label><input type="checkbox" id="user-catalog" name="Admission[catalog]" value="1" <?=$admission->catalog == 1 ? "checked":"";?> > - Категории продукции</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-products">
                        <input type="hidden" name="Admission[products]" value="0">
                        <label><input type="checkbox" id="user-products" name="Admission[products]" value="1" <?=$admission->products == 1 ? "checked":"";?> > - Продукция</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-country">
                        <input type="hidden" name="Admission[country]" value="0">
                        <label><input type="checkbox" id="user-country" name="Admission[country]" value="1" <?=$admission->country == 1 ? "checked":"";?> > - Страны </label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-geocoords">
                        <input type="hidden" name="Admission[geocoords]" value="0">
                        <label><input type="checkbox" id="user-geocoords" name="Admission[geocoords]" value="1" <?=$admission->geocoords == 1 ? "checked":"";?> > -  Координаты </label>
                        <div class="help-block"></div>
                    </div>



                    <div class="form-group field-admission-promocode">
                        <input type="hidden" name="Admission[promocode]" value="0">
                        <label><input type="checkbox" id="user-promocode" name="Admission[promocode]" value="1" <?=$admission->promocode == 1 ? "checked":"";?> > -  Промокоды </label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-gift">
                        <input type="hidden" name="Admission[gift]" value="0">
                        <label><input type="checkbox" id="user-gift" name="Admission[gift]" value="1" <?=$admission->gift == 1 ? "checked":"";?> > -  Подарки</label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-discount">
                        <input type="hidden" name="Admission[discount]" value="0">
                        <label><input type="checkbox" id="user-discount" name="Admission[discount]" value="1" <?=$admission->discount == 1 ? "checked":"";?> > -  Скидка для пенсионеров </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-free_delivery">
                        <input type="hidden" name="Admission[free_delivery]" value="0">
                        <label><input type="checkbox" id="user-free_delivery" name="Admission[free_delivery]" value="1" <?=$admission->free_delivery == 1 ? "checked":"";?>> -  Бесплатная доставка </label>
                        <div class="help-block"></div>
                    </div>
                </div>



                <label class="control-label" for="user-username">Триггерные письма</label>
                <div style="margin-left: 20px;">
                    <div class="form-group field-admission-trigger_birthday">
                        <input type="hidden" name="Admission[trigger_birthday]" value="0">
                        <label><input type="checkbox" id="user-trigger_birthday" name="Admission[trigger_birthday]" value="1" <?=$admission->trigger_birthday == 1 ? "checked":"";?> > -   В дни рождение </label>
                        <div class="help-block"></div>
                    </div>


                    <div class="form-group field-admission-trigger_holiday">
                        <input type="hidden" name="Admission[trigger_holiday]" value="0">
                        <label><input type="checkbox" id="user-trigger_holiday" name="Admission[trigger_holiday]" value="1" <?=$admission->trigger_holiday == 1 ? "checked":"";?> > -    В празднике</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-trigger_tovar_month">
                        <input type="hidden" name="Admission[trigger_tovar_month]" value="0">
                        <label><input type="checkbox" id="user-trigger_tovar_month" name="Admission[trigger_tovar_month]" value="1" <?=$admission->trigger_tovar_month == 1 ? "checked":"";?>>  -  При обновлении Товар Месяца</label>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-admission-trigger_remind_basket">
                        <input type="hidden" name="Admission[trigger_remind_basket]" value="0">
                        <label><input type="checkbox" id="user-trigger_remind_basket" name="Admission[trigger_remind_basket]" value="1" <?=$admission->trigger_remind_basket == 1 ? "checked":"";?> >  -  При не осущестивление покупку</label>
                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="form-group field-admission-orders">
                    <input type="hidden" name="Admission[orders]" value="0">
                    <label><input type="checkbox" id="user-orders" name="Admission[orders]" value="1" <?=$admission->orders == 1 ? "checked":"";?> >   Заказы</label>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-admission-contact">
                    <input type="hidden" name="Admission[contact]" value="0">
                    <label><input type="checkbox" id="user-contact" name="Admission[contact]" value="1" <?=$admission->contact == 1 ? "checked":"";?> >   Контакты</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-logo">
                    <input type="hidden" name="Admission[logo]" value="0">
                    <label><input type="checkbox" id="user-logo" name="Admission[logo]" value="1" <?=$admission->logo == 1 ? "checked":"";?> >   Логотип и Копирайт</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-text">
                    <input type="hidden" name="Admission[text]" value="0">
                    <label><input type="checkbox" id="user-text" name="Admission[text]" value="1" <?=$admission->text == 1 ? "checked":"";?> >   Текст по сайту</label>
                    <div class="help-block"></div>
                </div>


                <div class="form-group field-admission-review">
                    <input type="hidden" name="Admission[review]" value="0">
                    <label><input type="checkbox" id="user-review" name="Admission[review]" value="1" <?=$admission->review == 1 ? "checked":"";?> >   Заявки</label>
                    <div class="help-block"></div>
                </div>


            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <? if($from != "update"):?>

                    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

                <? endif;?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
