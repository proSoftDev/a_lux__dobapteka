<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Доступы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать администратор', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->getCreatedDate();
                },
                'format' => 'raw',
            ],





            ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
