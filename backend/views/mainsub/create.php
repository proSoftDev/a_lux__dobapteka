<?php

use yii\helpers\Html;

$this->title = 'Создание Mainsub';
$this->params['breadcrumbs'][] = ['label' => 'Mainsubs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mainsub-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
