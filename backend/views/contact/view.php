<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'telephone',
                'value' => $model->telephone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'address',
                'value' => $model->address,
                'format' => 'raw',
            ],
            [
                'attribute' => 'instagram',
                'value' => $model->instagram,
                'format' => 'raw',
            ],
            [
                'attribute' => 'facebook',
                'value' => $model->facebook,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
