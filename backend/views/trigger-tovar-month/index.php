<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' При обновлении Товар Месяца';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trigger-tovar-month-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
