<?php

use yii\helpers\Html;

$this->title = 'Создание Trigger Tovar Month';
$this->params['breadcrumbs'][] = ['label' => 'Trigger Tovar Months', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trigger-tovar-month-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
