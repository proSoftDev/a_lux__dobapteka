<?php

use yii\helpers\Html;

$this->title = 'Создание Trigger Remind Basket';
$this->params['breadcrumbs'][] = ['label' => 'Trigger Remind Baskets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trigger-remind-basket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
