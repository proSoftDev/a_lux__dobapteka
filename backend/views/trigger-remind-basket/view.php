<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'При не осущестивление покупку';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>
<div class="trigger-remind-basket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
