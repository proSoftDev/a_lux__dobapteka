<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= Html::a('Редактирование', ['update', 'id' => $id], ['class' => 'btn btn-primary']) ?>
    <div class="box" id="photos">
        <div class="box-header with-border" id="">catalog</div>
        <div class="box-body">
            <div class="row">
                <?php foreach ($models as $v): ?>
                    <div class="col-md-2 col-xs-3" style="text-align: center">
                        <div style="margin-top:20px;">
                            <img src="/backend/web/<?= $v->path.$v->image?>" alt="" style="width: 80px;height: 80px;">
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
