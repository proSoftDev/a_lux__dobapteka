<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="catalog-form">
    <div class="row">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data'], 'action' => ['catalog/update', 'id' => $id]
    ]); ?>
<?php  foreach ($model as $v): ?>
    <div class="col-md-2 col-xs-3" style="text-align: center">
        <div class="btn-group">
            <?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span>', ['catalog/move-up', 'id' => $v->id, 'menu_id' => $v->category_id], [
                'class' => 'btn btn-default',
                'data-method' => 'post',
            ]); ?>
            <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['catalog/delete', 'id' => $v->id, 'menu_id' => $v->category_id], [
                'class' => 'btn btn-default',
                'data-method' => 'post',
                'data-confirm' => 'Remove photo?',
            ]); ?>
            <?= Html::a('<span class="glyphicon glyphicon-arrow-right"></span>', ['catalog/move-down', 'id' => $v->id, 'menu_id' => $v->category_id], [
                'class' => 'btn btn-default',
                'data-method' => 'post',
            ]); ?>
        </div>
        <div style="margin-top:20px;">
            <img src="/backend/web/<?=$v->path.$v->image?>" alt="" style="width: 80px;height: 80px;">
        </div>
        <?= $form->field($v, 'id[]')->hiddenInput(['value' => $v->id]) ?>
        <?= $form->field($v, 'name[]')->textarea(['value' => $v->name]) ?>
    </div>
<?php  endforeach; ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
        <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data'], 'action' => ['catalog/create', 'id' => $id]
        ]); ?>

        <?=$form->field($new_model, 'files[]')->label(false)->widget(FileInput::className(), [
        'options' => [
        'accept' => 'image/*',
        'multiple' => true,
        ]
        ]) ?>

        <div class="form-group">
            <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
