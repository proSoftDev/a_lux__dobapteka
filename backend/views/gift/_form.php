<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

$products = \common\models\Products::find()->all();
$my_products = $model->getAvailableProductsAsArray();
?>

<?php if(Yii::$app->session->getFlash('products_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('products_error'); ?>
    </div>
<?php endif;?>

<?php if(Yii::$app->session->getFlash('price_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('price_error'); ?>
    </div>
<?php endif;?>
<div class="gift-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?>

    <?= $form->field($model, 'product_id')->dropDownList($model->getProducts()) ?>

    <?= $form->field($model, 'from_price')->textInput() ?>

    <?= $form->field($model, 'to_price')->textInput() ?>

    <div class="form-group">
        <label>Продукты</label>
        <select class="form-control select2" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="Gift[products][]">
            <? foreach ($products as $v):?>
                <option value="<?=$v->id;?>" <?=$my_products[$v->id] == $v->id?"selected":"";?>><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    if($('#gift-type').val() == 0){
        $('.field-gift-from_price').hide();
        $('.field-gift-to_price').hide();
        $('.field-gift-product_id').show();
    }else{
        $('.field-gift-from_price').show();
        $('.field-gift-to_price').show();
        $('.field-gift-product_id').hide();
    }

    $('#gift-type').change(function(){
        if($(this).val() == 0){
            $('.field-gift-from_price').hide();
            $('.field-gift-to_price').hide();
            $('.field-gift-product_id').show();
        }else{
            $('.field-gift-from_price').show();
            $('.field-gift-to_price').show();
            $('.field-gift-product_id').hide();
        }
    });
</script>