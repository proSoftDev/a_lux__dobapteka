<?php

use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',

            [
                'attribute' => 'description',
                'value' => $model->description,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => LabelPopular::statusList(),
                'value' => function ($model) {
                    return LabelPopular::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            'price',
            'discount',
            [
                'attribute' => 'url',
                'value' => $model->url,
                'format' => 'raw',
            ],
            [
                'attribute' => 'images',
                'value' => $model->images,
                'format' => 'raw',
            ],
            'sort',
            [
                'attribute' => 'status',
                'filter' => \backend\controllers\LabelNew::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelNew::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'status_products',
            'created_at',

        ],
    ]) ?>

    <table id="w1" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Сопутствующие товары</th>
                <td>
                <? foreach ($sop_tovary as $v):?>
                    <?=$v->product->name.'<br>';?>
                <? endforeach;?>
                </td>
            </tr>
            <tr>
                <th>Аналоги лекарственных средств</th>
                <td>
                    <? foreach ($analogy as $v):?>
                        <?=$v->product->name.'<br>';?>
                    <? endforeach;?>
                </td>
            </tr>
        </tbody>
    </table>

</div>
