<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

?>
<div class="products-form">
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false
        ]),
    ]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="" style="text-align: center">
            <?$images = unserialize($model->images)?>
            <div style="margin-top:20px;">
                <?if($model->images)foreach($images as $v){?>
                    <img src="/backend/web/<?=$model->path.$v?>" alt="" style="margin-left:15px;width:400px;height:400px;float:left;">
                <?}?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'files[]')->label(false)->widget(FileInput::className(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ]
    ]) ?>
    <div class="form-group field-products-category_id required has-error">
        <label class="control-label" for="products-category_id">Category ID</label>

        <select name="Products[category_id]" class="form-control">
            <option></option>
        <?
            $catalog = \common\models\CatalogProducts::find()
                ->where("level = 1")->all();
            $array = [];
            foreach ($catalog as $v){
                echo '<option value="'.$v->id.'" disabled="disabled">'.$v->name.'</option>';
                $array[$v->id] = $v->name;
                if(isset($v->childs)){
                    foreach($v->childs as $child1) {
                        if(!empty($child1->childs))
                            echo '<option value="'.$child1->id.'" disabled="disabled">--'.$child1->name.'</option>';
                        else{
                            $selected = '';
                            if($child1->id == $model->category_id)
                                $selected = 'selected="selected"';
                                echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                        }
                        $array[$child1->id] = '--'.$child1->name;
                        if(isset($child1->childs)){
                            foreach($child1->childs as $child2){
                                $selected = '';
                                if($child2->id == $model->category_id)
                                    $selected = 'selected="selected"';
                                echo '<option '.$selected.' value="'.$child2->id.'">----'.$child2->name.'</option>';
                                $array[$child2->id] = '----'.$child2->name;
                            }
                        }
                    }
                }
            }
        ?>
        </select>

    </div>
    <?//= $form->field($model, 'category_id')->dropDownList($array ,['prompt' => '']) ?>
    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id' ,'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'top_month')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isNew')->dropDownList([1 => 'Новые',0 => 'Старые']) ?>

    <?= $form->field($model, 'isHit')->dropDownList([0 => 'Нет',1 => 'Да'])?>

    <?= $form->field($model, 'bonus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно'], ['prompt' => '']) ?>

    <?= $form->field($model, 'status_products')->dropDownList(['дефицит' => 'дефицит', null => null]) ?>


    <div class="form-group">
        <label> Сопутствующие товары</label>
        <select class="form-control sop_tovary" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="sop_tovary[]">
            <? foreach ($products as $v):?>
                <option value="<?=$v->id;?>" <?=$sop_tovary[$v->id] == $v->id?"selected":"";?>><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>


    <div class="form-group">
        <label> Аналоги лекарственных средств</label>
        <select class="form-control analogy" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="analogy[]">
            <? foreach ($products as $v):?>
                <option value="<?=$v->id;?>" <?=$analogy[$v->id] == $v->id?"selected":"";?>><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>



    <div class="form-group" style="padding-top: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
