<?php

use yii\helpers\Html;

$this->title = 'Создание Discount Earliest Persons';
$this->params['breadcrumbs'][] = ['label' => 'Discount Earliest Persons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-earliest-persons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
