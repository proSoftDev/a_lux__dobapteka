<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Скидка для пенсионеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-earliest-persons-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать Discount Earliest Persons', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'attribute' => 'age',
                'value' => function($data){
                    return $data->age. ' лет';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'discount',
                'value' => function($data){
                    return $data->discount. '%';
                }
            ],

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
