<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Категории продукции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    return
                        Html::a($model->parent->name, ['view', 'id' => $model->parent->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'title',
                'value' => $model->title,
                'format' => 'raw',
            ],
            [
                'attribute' => 'description',
                'value' => $model->description,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                 'value' => function ($model) {
                     return
                         Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                         Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
