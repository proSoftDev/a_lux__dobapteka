<?php

use yii\helpers\Html;

$this->title = 'Редактирование категория: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Catalog Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="catalog-products-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
