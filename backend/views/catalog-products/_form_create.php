<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="catalog-products-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-4 col-xs-3" style="text-align: center">
            <div style="margin-top:20px;">
                <img src="/backend/web/<?=$model->path.$model->img?>" alt="" style="width:400px;">
            </div>
        </div>
    </div>

    <?= $form->field($model, 'img')->label(false)->widget(FileInput::className(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => false,
        ]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно'], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
