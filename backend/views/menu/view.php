<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <? if($model->isNew):?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'text',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return Label::statusLabel($model->status);
                    },
                    'format' => 'raw',
                ],
                [
                    'format' => 'raw',
                    'attribute' => 'content',
                    'value' => function($data){
                        return $data->content;
                    }
                ],
                'metaName',
                'metaDesc:ntext',
                'metaKey:ntext',
                'url',
                [
                    'format' => 'raw',
                    'attribute' => 'icon',
                    'value' => function($data){
                        return '<i class="'.$data->icon.'" aria-hidden="true"></i>';
                    }
                ],
            ],
        ]) ?>
    <? else:?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'text',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return Label::statusLabel($model->status);
                    },
                    'format' => 'raw',
                ],
                'metaName',
                'metaDesc:ntext',
                'metaKey:ntext',
                [
                    'format' => 'raw',
                    'attribute' => 'icon',
                    'value' => function($data){
                        return '<i class="'.$data->icon.'" aria-hidden="true"></i>';
                    }
                ],

            ],
        ]) ?>
    <? endif;?>

</div>
