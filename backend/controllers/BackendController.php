<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2019
 * Time: 15:32
 */

namespace backend\controllers;


use common\models\Admission;
use common\models\Review;
use Yii;
use yii\web\Controller;

class BackendController extends Controller
{

    public function beforeAction($action){
        if(\Yii::$app->user->identity->role != 1 && \Yii::$app->user->identity->role != 2 && $action->actionMethod != 'actionLogin'){
            \Yii::$app->user->logout();
            return $this->redirect('/admin/site/login');
        }


        Yii::$app->view->params['admission'] = Admission::findOne(["user_id" => Yii::$app->user->id]);


        Yii::$app->view->params['feedback'] = Review::findAll(['isRead' => 0]);
        return parent::beforeAction($action);

    }

}
