<?php require_once 'header.php'?>
<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-title text-center gradient-text">
                            <h5>Наши аптеки</h5>
                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <div class="search">
                            <input type="text" placeholder="Поиск">
                        </div>
                        <div class="checkbox-flex" style="margin: 30px 0;">
                            <label class="filter-checkbox">Открыто
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>

                        </div>
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39108.459878219815!2d76.9290723713175!3d43.23562398492848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1556599904432!5m2!1sru!2skz" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="drugstore-wrap">
                                    <div class="drugstore-item">
                                        <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                        <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                        <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                        <div class="status-open">
                                            Открыто
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="drugstore-wrap">
                                    <div class="drugstore-item">
                                        <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                        <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                        <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                        <div class="status-close">
                                            Закрыто
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-title text-center gradient-text">
                            <h5>Наши аптеки</h5>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="search">
                            <input type="text" placeholder="Поиск">
                        </div>
                        <div class="checkbox-flex">
                            <label class="filter-checkbox">Открыто
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>

                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39108.459878219815!2d76.9290723713175!3d43.23562398492848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1556599904432!5m2!1sru!2skz" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="drugstore-wrap">
                                    <div class="drugstore-item">
                                        <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                        <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                        <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                        <div class="status-open">
                                            Открыто
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="drugstore-wrap">
                                    <div class="drugstore-item">
                                        <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                        <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                        <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                        <div class="status-close">
                                            Закрыто
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
</div>
<?php require_once 'footer.php'?>
