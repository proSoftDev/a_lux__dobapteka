<?php require_once 'header.php'?>
    <div class="main">
       <div class="desktop-version">
           <div class="slider">
               <div class="container">
                   <div class="row">
                       <div class="col-lg-3 col-md-4 col-sm-12 wow fadeInLeft">
                           <div class="catalog-menu">
                               <div class="title gradient-text">
                                   Каталог товаров
                               </div>
                               <ul>
                                   <li>
                                       <a href=""><span class="menu-icon"><img src="images/menu11.png" alt=""></span><p>Лекарственные средства</p></a>
                                       <div class="modal-drug">
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                       </div>
                                   </li>
                                   <li>
                                       <a href=""><span class="menu-icon"><img src="images/menu21.png" alt=""></span><p>Витамины и бады</p></a>
                                       <div class="modal-drug">
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие2</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                       </div>
                                   </li>
                                   <li>
                                       <a href=""><span class="menu-icon"><img src="images/menu31.png" alt=""></span><p>Красота и гигиена</p></a>
                                       <div class="modal-drug">
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие3</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                       </div>
                                   </li>
                                   <li>
                                       <a href=""><span class="menu-icon"><img src="images/menu41.png" alt=""></span><p>Мать и детя</p></a>
                                       <div class="modal-drug">
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие4</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                       </div>
                                   </li>
                                   <li>
                                       <a href=""><span class="menu-icon"><img src="images/menu51.png" alt=""></span><p>Изделия мед. назначения</p></a>
                                       <div class="modal-drug">
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие3</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                           <ul class="second-level-menu">
                                               <li><a href="">Жаропонижающие</a></li>
                                               <li><a href="">Пробиотики и пребиотики</a></li>
                                               <li><a href="">Противовирусные препараты</a></li>
                                               <li><a href="">Рецептурные препараты</a></li>
                                               <li><a href="">Слабительные препараты</a></li>
                                               <li><a href="">Спазмолитические препараты</a></li>
                                               <li><a href="">Лечение заболеваний мочеполовой системы</a></li>
                                           </ul>
                                       </div>
                                   </li>
                               </ul>
                           </div>
                       </div>
                       <div class="col-lg-9 col-md-8 col-sm-12 wow fadeInRight">
                           <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                               <ol class="carousel-indicators">
                                   <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                   <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                   <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                   <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                   <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                               </ol>
                               <div class="carousel-inner">
                                   <div class="carousel-item active">
                                       <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                       <div class="carousel-caption">
                                           <h5 class="gradient-text">20%</h5>
                                           <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                       </div>
                                       <div class="green-caption">
                                           <img src="images/green-caption.png" class="img-fluid" alt="">
                                       </div>
                                   </div>
                                   <div class="carousel-item">
                                       <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                       <div class="carousel-caption">
                                           <h5 class="gradient-text">20%</h5>
                                           <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                       </div>
                                       <div class="green-caption">
                                           <img src="images/green-caption.png" class="img-fluid" alt="">
                                       </div>
                                   </div>
                                   <div class="carousel-item">
                                       <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                       <div class="carousel-caption">
                                           <h5 class="gradient-text">20%</h5>
                                           <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                       </div>
                                       <div class="green-caption">
                                           <img src="images/green-caption.png" class="img-fluid" alt="">
                                       </div>
                                   </div>
                                   <div class="carousel-item">
                                       <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                       <div class="carousel-caption">
                                           <h5 class="gradient-text">20%</h5>
                                           <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                       </div>
                                       <div class="green-caption">
                                           <img src="images/green-caption.png" class="img-fluid" alt="">
                                       </div>
                                   </div>
                                   <div class="carousel-item">
                                       <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                       <div class="carousel-caption">
                                           <h5 class="gradient-text">20%</h5>
                                           <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                       </div>
                                       <div class="green-caption">
                                           <img src="images/green-caption.png" class="img-fluid" alt="">
                                       </div>
                                   </div>

                               </div>
                               <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                   <img src="images/left.png" alt="">
                                   <span class="sr-only">Previous</span>
                               </a>
                               <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                   <img src="images/right.png" alt="">
                                   <span class="sr-only">Next</span>
                               </a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <div class="main-content">
               <div class="container">
                   <div class="row">
                       <div class="col-sm-12">
                           <div class="main-title gradient-text wow fadeInUp">
							   <p>Наши преимущества</p>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                           <div class="adv-item">
                               <a href="">
                                   <div class="adv-icon">
                                   <img src="images/advantage-icon1.png" alt="">
                               </div>
                               <div class="text-block">
                                   <h6>Надежность</h6>
                                   <p>Покупай лекарства - получай скидки и бонусы</p>
                               </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                           <div class="adv-item">
                               <a href="">
                                   <div class="adv-icon">
                                   <img src="images/advantage-icon2.png" alt="">
                               </div>
                               <div class="text-block">
                                   <h6>Бонусы</h6>
                                   <p>Получайте бонусы после каждой покупки!</p>
                               </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                           <div class="adv-item">
                               <a href="">
                                   <div class="adv-icon">
                                   <img src="images/advantage-icon3.png" alt="">
                               </div>
                               <div class="text-block">
                                   <h6>Доставка</h6>
                                   <p>Бесплатная доставка при покупке на сумму 20 тыс.</p>
                               </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                           <div class="adv-item">
                               <a href="">
                                   <div class="adv-icon">
                                   <img src="images/advantage-icon4.png" alt="">
                               </div>
                               <div class="text-block">
                                   <h6> Система подарков</h6>
                                   <p>Мы любим наших клиентов и дарим им подарки</p>
                               </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12">
                           <div class="title-border"></div>
                           <div class="main-title gradient-text wow fadeInUp">
                               <h5>Топ продаж</h5>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                           <div class="top-sale-item">
                               <a href="product-card.php">
                                   <div class="hit">
                                       <img src="images/hit.png" alt="">
                                   </div>
                                   <img src="images/img3.png" alt="">
                                   <div class="name">
                                       <p>Doliva маска расслабляющая 30 мл</p>
                                   </div>
                                   <div class="price">
                                       <p class="gradient-text">369 <span>тг.</span></p>
                                   </div>
                                   <div class="basket-button">
                                       <button><img src="images/light-basket.png" alt="">В корзину</button>
                                   </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                           <div class="top-sale-item">
                               <a href="product-card.php">
                                   <div class="hit">
                                       <img src="images/hit.png" alt="">
                                   </div>
                                   <img src="images/img3.png" alt="">
                                   <div class="name">
                                       <p>Doliva маска расслабляющая 30 мл</p>
                                   </div>
                                   <div class="price">
                                       <p class="gradient-text">369 <span>тг.</span></p>
                                   </div>
                                   <div class="basket-button">
                                       <button><img src="images/light-basket.png" alt="">В корзину</button>
                                   </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                           <div class="top-sale-item">
                               <a href="product-card.php">
                                   <div class="hit">
                                       <img src="images/hit.png" alt="">
                                   </div>
                                   <img src="images/img3.png" alt="">
                                   <div class="name">
                                       <p>Doliva маска расслабляющая 30 мл</p>
                                   </div>
                                   <div class="price">
                                       <p class="gradient-text">369 <span>тг.</span></p>
                                   </div>
                                   <div class="basket-button">
                                       <button><img src="images/light-basket.png" alt="">В корзину</button>
                                   </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                           <div class="top-sale-item">
                               <a href="product-card.php">
                                   <div class="hit">
                                       <img src="images/hit.png" alt="">
                                   </div>
                                   <img src="images/img3.png" alt="">
                                   <div class="name">
                                       <p>Doliva маска расслабляющая 30 мл</p>
                                   </div>
                                   <div class="price">
                                       <p class="gradient-text">369 <span>тг.</span></p>
                                   </div>
                                   <div class="basket-button">
                                       <button><img src="images/light-basket.png" alt="">В корзину</button>
                                   </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12">
                           <div class="title-border"></div>
                           <div class="main-title gradient-text wow fadeInUp">
                               <h5>Новинки</h5>
                           </div>
                       </div>
                       <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                           <div class="link-wrap">
                               <a href="product-card.php">
                                   <div class="new-item">
                               <div class="image">
                                   <img src="images/img1.png" alt="">
                               </div>
                               <div class="text-block">
                                   <div class="name">
                                       <p>Йодомарин 10 табл.</p>
                                   </div>
                                   <div class="status">
                                       <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                   </div>
                                   <div class="flex-one">
                                       <div class="price">
                                           <p class="gradient-text">369 <span>тг.</span></p>
                                       </div>
                                       <div class="news-basket-button">
                                           <button><img src="images/light-basket.png" alt="">В корзину</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                           <div class="link-wrap">
                               <a href="product-card.php">
                                   <div class="new-item">
                               <div class="image">
                                   <img src="images/img4.png" alt="">
                               </div>
                               <div class="text-block">
                                   <div class="name">
                                       <p>Аллохол №50, табл., покрытые оболочкой</p>
                                   </div>
                                   <div class="status">
                                       <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                   </div>
                                   <div class="flex-one">
                                       <div class="price">
                                           <p class="gradient-text">230 <span>тг.</span></p>
                                       </div>
                                       <div class="news-basket-button">
                                           <button><img src="images/light-basket.png" alt="">В корзину</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                           <div class="link-wrap">
                               <a href="product-card.php">
                                   <div class="new-item">
                                       <div class="image">
                                           <img src="images/img3.png" alt="">
                                       </div>
                                       <div class="text-block">
                                           <div class="name">
                                               <p>Энтерожермина 5 мл</p>
                                           </div>
                                           <div class="status">
                                               <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                           </div>
                                           <div class="flex-one">
                                               <div class="price">
                                                   <p class="gradient-text">1200 <span>тг.</span></p>
                                               </div>
                                               <div class="news-basket-button">
                                                   <button><img src="images/light-basket.png" alt="">В корзину</button>
                                               </div>
                                           </div>
                                       </div>
                                    </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                           <div class="link-wrap">
                               <a href="product-card.php">
                                   <div class="new-item">
                               <div class="image">
                                   <img src="images/img1.png" alt="">
                               </div>
                               <div class="text-block">
                                   <div class="name">
                                       <p>Йодомарин 10 табл.</p>
                                   </div>
                                   <div class="status">
                                       <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                   </div>
                                   <div class="flex-one">
                                       <div class="price">
                                           <p class="gradient-text">369 <span>тг.</span></p>
                                       </div>
                                       <div class="news-basket-button">
                                           <button><img src="images/light-basket.png" alt="">В корзину</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                               </a>
                           </div>
                       </div>
                       <div class="col-sm-12">
                           <div class="all-catalog wow fadeInUp">
                               <button>Весь каталог</button>
                           </div>
                           <div class="title-border"></div>
                           <div class="main-title gradient-text">
                               <h5>Как нас найти</h5>
                           </div>
                       </div>
                       <div class="col-sm-12">
                           <div class="map-block">
                               <div class="map wow fadeInLeft">
                                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39108.459878219815!2d76.9290723713175!3d43.23562398492848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1556599904432!5m2!1sru!2skz" frameborder="0"  allowfullscreen></iframe>
                               </div>
                               <div class="drugstore-list wow fadeInRight">
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-open">
                                           Открыто
                                       </div>
                                   </div>
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-close">
                                           Закрыто
                                       </div>
                                   </div>
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-open">
                                           Открыто
                                       </div>
                                   </div>
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-close">
                                           Закрыто
                                       </div>
                                   </div>
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-open">
                                           Открыто
                                       </div>
                                   </div>
                                   <div class="drugstore-item">
                                       <p><img src="images/loc-icon.png" alt=""><b>ул. Малика Габдуллина 7</b></p>
                                       <p><img src="images/clock.png" alt="">12:00 - 24:00 ч.</p>
                                       <p><img src="images/phone-icon.png" alt="">+7 777 322 32 32</p>
                                       <div class="status-close">
                                           Закрыто
                                       </div>
                                   </div>
                               </div>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
<!--           <div class="pre-footer">-->
<!--               <div class="container">-->
<!--                   <div class="row wow fadeInUp">-->
<!--                       <div class="col-sm-2">-->
<!--                          <div class="logo">-->
<!--                              <a href="">-->
<!--                                  <img src="images/logo.png" alt="">-->
<!--                              </a>-->
<!--                          </div>-->
<!--                       </div>-->
<!--                       <div class="col-sm-2">-->
<!--                           <div class="loc">-->
<!--                               <div class="loc-icon">-->
<!--                                   <img src="images/loc-icon.png" alt="">-->
<!--                               </div>-->
<!--                               <p>Алматы</p>-->
<!--                           </div>-->
<!--                       </div>-->
<!---->
<!--                       <div class="offset-lg-6 col-sm-2">-->
<!--                           <div class="loc">-->
<!--                               <div class="loc-icon">-->
<!--                                   <img src="images/phone-icon.png" alt="">-->
<!--                               </div>-->
<!--                               <a href="tel:+7 777 352 55 45"><p>+7 777 352 55 45</p></a>-->
<!--                           </div>-->
<!--                       </div>-->
<!---->
<!--                   </div>-->
<!--               </div>-->
<!--           </div>-->
       </div>
        <div class="mobile-version">
            <div class="slider">
                <div class="container">
                    <div class="row wow fadeInUp">
                        <div class="col-sm-12">
                            <div class="owl1 owl-carousel owl-theme">
                                <div class="item">
                                    <div class="green" >
                                        <a href="#tab">Лекарственные средства</a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="green">
                                        <a href="#tab2">Витамины и бады</a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="green">
                                        <a href="#tab3">Лекарственные средства</a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="green">
                                        <a href="#tab4">Лекарственные средства</a>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="green">
                                        <a href="#tab5">Лекарственные средства</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div id="carouselExampleIndicators2" class="desktop-version carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="4"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100 img-fluid" src="images/slide.png" alt="First slide">
                                        <div class="carousel-caption">
                                            <h5 class="gradient-text">20%</h5>
                                            <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                        </div>
                                        <div class="green-caption">
                                            <img src="images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                        <div class="carousel-caption">
                                            <h5 class="gradient-text">20%</h5>
                                            <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                        </div>
                                        <div class="green-caption">
                                            <img src="images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                        <div class="carousel-caption">
                                            <h5 class="gradient-text">20%</h5>
                                            <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                        </div>
                                        <div class="green-caption">
                                            <img src="images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                        <div class="carousel-caption">
                                            <h5 class="gradient-text">20%</h5>
                                            <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                        </div>
                                        <div class="green-caption">
                                            <img src="images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/slide.png" alt="First slide">
                                        <div class="carousel-caption">
                                            <h5 class="gradient-text">20%</h5>
                                            <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                        </div>
                                        <div class="green-caption">
                                            <img src="images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>

                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                    <img src="images/left.png" alt="">
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                    <img src="images/right.png" alt="">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="owl-main owl-carousel owl-theme mobile-version">
                                <div class="item">
                                    <img class="d-block w-100 img-fluid" src="images/slide.png" alt="First slide">
                                    <div class="carousel-caption">
                                        <h5 class="gradient-text">20%</h5>
                                        <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                    </div>
                                    <div class="green-caption">
                                        <img src="images/green-caption.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <img class="d-block w-100 img-fluid" src="images/slide.png" alt="First slide">
                                    <div class="carousel-caption">
                                        <h5 class="gradient-text">20%</h5>
                                        <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                    </div>
                                    <div class="green-caption">
                                        <img src="images/green-caption.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <img class="d-block w-100 img-fluid" src="images/slide.png" alt="First slide">
                                    <div class="carousel-caption">
                                        <h5 class="gradient-text">20%</h5>
                                        <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                    </div>
                                    <div class="green-caption">
                                        <img src="images/green-caption.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <img class="d-block w-100 img-fluid" src="images/slide.png" alt="First slide">
                                    <div class="carousel-caption">
                                        <h5 class="gradient-text">20%</h5>
                                        <p>до <b>29.02</b> на любой <br>продукт фирмы <span class="gradient-text">“Life”</span></p>
                                    </div>
                                    <div class="green-caption">
                                        <img src="images/green-caption.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="container">
                    <div class="row wow fadeInUp">
                        <div class="col-sm-12">
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Наши преимущества</h5>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="square-flex">
                                <div class="adv-item">
                                    <a href="">
                                        <div class="adv-icon">
                                        <img src="images/advantage-icon1.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6>Надежность</h6>
                                        <p>Покупай лекарства - получай скидки и бонусы</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="adv-item">
                                    <a href="">
                                        <div class="adv-icon">
                                        <img src="images/advantage-icon2.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6>Бонусы</h6>
                                        <p>Получайте бонусы после каждой покупки!</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="adv-item">
                                    <a href="">
                                        <div class="adv-icon">
                                        <img src="images/advantage-icon3.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6>Доставка</h6>
                                        <p>Бесплатная доставка при покупке на сумму 20 тыс.</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="adv-item">
                                    <a href="">
                                        <div class="adv-icon">
                                        <img src="images/advantage-icon4.png" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6> Система подарков</h6>
                                        <p>Мы любим наших клиентов и дарим им подарки</p>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Топ продаж</h5>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp ">
                            <div class="square-flex">
                                <div class="top-sale-item">
                                    <div class="hit">
                                        <img src="images/hit.png" alt="">
                                    </div>
                                    <img src="images/img1.png" alt="">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span>тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                                <div class="top-sale-item">
                                    <div class="hit">
                                        <img src="images/hit.png" alt="">
                                    </div>
                                    <img src="images/img3.png" alt="">
                                    <div class="name">
                                        <p>Doliva маска расслабляющая 30 мл</p>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text">369 <span>тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button><img src="images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp ">
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Новинки</h5>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                            <div class="new-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span>тг.</span></p>
                                        </div>
                                        <div class="news-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                            <div class="new-item">
                                <div class="image">
                                    <img src="images/img4.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="name">
                                        <p>Аллохол №50, табл., покрытые оболочкой</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">230 <span>тг.</span></p>
                                        </div>
                                        <div class="news-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                            <div class="new-item">
                                <div class="image">
                                    <img src="images/img3.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="name">
                                        <p>Энтерожермина 5 мл</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">1200 <span>тг.</span></p>
                                        </div>
                                        <div class="news-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                            <div class="new-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span>тг.</span></p>
                                        </div>
                                        <div class="news-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="all-catalog">
                                <button>Весь каталог</button>
                            </div>
                            <div class="title-border"></div>
                            <div class="main-title gradient-text">
                                <h5>Как нас найти</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map wow fadeInUp">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39108.459878219815!2d76.9290723713175!3d43.23562398492848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e7d16c5cbab%3A0x3d44668fad986d76!2z0JDQu9C80LDRgtGL!5e0!3m2!1sru!2skz!4v1556599904432!5m2!1sru!2skz"  frameborder="0"  allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>
<?php require_once 'footer.php'?>
