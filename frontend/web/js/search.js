$(document).ready(function() {
    $( "#searchbox" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        // focus: function(event, ui) {
        //     $('#searchbox').val(ui.item.label);
        //     return false;
        // },
        select: function(event, ui) {
            $('#searchbox').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $( "#searchboxFixed" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        // focus: function(event, ui) {
        //     $('#searchboxFixed').val(ui.item.label);
        //     return false;
        // },
        select: function(event, ui) {
            $('#searchboxFixed').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $( "#searchboxMobile" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        // focus: function(event, ui) {
        //     $('#searchboxMobile').val(ui.item.label);
        //     return false;
        // },
        select: function(event, ui) {
            $('#searchboxMobile').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });



    $( "#searchboxMobileFixed" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/card/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        // focus: function(event, ui) {
        //     $('#searchboxMobileFixed').val(ui.item.label);
        //     return false;
        // },
        select: function(event, ui) {
            $('#searchboxMobileFixed').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });




});
