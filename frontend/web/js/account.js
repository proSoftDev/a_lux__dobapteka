

$('input[name="UserProfile[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="UserProfile[surname]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});


$('input[name="UserProfile[father]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});



$('body').on('click', '.register-button', function (e) {

    var button = $(this).html();
    showButtonLoader('.register-button');
    phoneStatus = true;
    var phone = $(".telephone").val();
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.register-button', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/account/register',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                hideButtonLoader('.register-button', button);
                if (response == 1) {
                    window.location.href = "/account/?tab=bonus";
                } else {
                    $('#my-captcha-image').yiiCaptcha('refresh');
                    swal('Ошибка!', response, 'error');
                }
            },
            error: function () {
                hideButtonLoader('.register-button', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});

$('body').on('click', '.register-button-mob', function (e) {

    var button = $(this).html();
    showButtonLoader('.register-button-mob');
    e.preventDefault();
    phoneStatus = true;
    var phone = $(".telephone-mobile").val();
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.register-button-mob', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else {
        $.ajax({
            type: 'POST',
            url: '/account/register',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                hideButtonLoader('.register-button-mob', button);
                if (response == 1) {
                    window.location.href = "/account/?tab=bonus";
                } else {
                    $('#my-captcha-image').yiiCaptcha('refresh');
                    swal('Ошибка!', response, 'error');
                }
            },
            error: function () {
                hideButtonLoader('.register-button-mob', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});





$('body').on('click', '.login-button', function (e) {

    var button = $(this).html();
    showButtonLoader('.login-button');
	
    phoneStatus = true;
    var phone = $(".telephone").val();

    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.login-button', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else {
        $.ajax({
            type: 'POST',
            url: '/account/login',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                hideButtonLoader('.login-button', button);
                if (response == 1) {
                    window.location.href = "/account/?tab=bonus";
                } else {
                    swal('Ошибка!', response, 'error');
                }
            },
            error: function () {
                hideButtonLoader('.login-button', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});


$('body').on('click', '.login-button-mobile', function (e) {

    var button = $(this).html();
    showButtonLoader('.login-button-mobile');
	
    phoneStatus = true;
    var phone = $(".telephone_mobile").val();

    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.login-button-mobile', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else {
        $.ajax({
            type: 'POST',
            url: '/account/login',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                hideButtonLoader('.login-button-mobile', button);
                if (response == 1) {
                    window.location.href = "/account/?tab=bonus";
                } else {
                    swal('Ошибка!', response, 'error');
                }
            },
            error: function () {
                hideButtonLoader('.login-button-mobile', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});



$('body').on('click', '.update-profile-button', function (e) {
    showLoader();
    phoneStatus = true;
    var phone = $(".telephone").val();

    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }

    if (!phoneStatus || phone == 0) {
        hideLoader();
        swal('', 'Необходимо заполнить «Телефон».', 'error');
    }else {
        $.ajax({
            type: 'POST',
            url: '/account/update-account-data',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                hideLoader();
                if (response == 1) {
                    $('input[name="User[password]"]').val("");
                    $('input[name="User[password_verify]"]').val("");
                    swal('Ваши изменения', ' успешно сохранен.', 'success');
                } else swal('', response, 'error');

            },
            error: function () {
                hideLoader();
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});


$(".btn-add-favorite").click(function () {
    var product_id = $(this).attr('data-id');
    $.ajax({
        url: "/account/add-to-favorite",
        dataType: "json",
        data: {product_id:product_id},
        type: "GET",
        success: function(data){
            if(data.status){
                alert(data.text);
            }else{
                swal('Ошибка!', data.text, 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});



$("body").on("click", ".btn-delete-product-from-favorite", function () {
    showLoader();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/account/delete-from-favorite",
        data: {id:id},
        type: "GET",
        success: function(data){
            hideLoader();
            if(data == 0) swal('Упс!', 'Что-то пошло не так.', 'error');
            else $('#favorite').html(data);
        },
        error: function () {
            hideLoader();
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '.confirm-telephone', function (e) {

    var button = $(this).html();
    showButtonLoader('.confirm-telephone');
    phoneStatus = true;
    var phone = $(".telephone").val();

    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.confirm-telephone', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else{
        $.ajax({
            type: 'GET',
            url: '/account/update-phone',
            data: {phone:phone},
            success: function (response) {
                if(response == 1) window.location.href = "/account/?tab=bonus";
                else swal('Ошибка!', response, 'error');
                hideButtonLoader('.confirm-telephone', button);
            },
            error: function () {
                hideButtonLoader('.confirm-telephone', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});



$('body').on('click', '.confirm-telephone-mobile', function (e) {

    var button = $(this).html();
    showButtonLoader('.confirm-telephone-mobile');
    phoneStatus = true;
    var phone = $(".telephone_mobile").val();

    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }
    if (!phoneStatus || phone == 0) {
        hideButtonLoader('.confirm-telephone-mobile', button);
        swal('Ошибка!', 'Необходимо заполнить «Телефон».', 'error');
    }else{
        $.ajax({
            type: 'GET',
            url: '/account/update-phone',
            data: {phone:phone},
            success: function (response) {
                if(response == 1) window.location.href = "/account/?tab=home";
                else swal('Ошибка!', response, 'error');
                hideButtonLoader('.confirm-telephone-mobile', button);
            },
            error: function () {
                hideButtonLoader('.confirm-telephone-mobile', button);
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});



$('body').on('click', '#gift-in-basket', function (e) {



    $.ajax({
        url: "/card/add-gift",
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.status) {
                $('.count').html(data.count);
                window.location.href = "/card/";
            }else{
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }

            $('.count-add').toggleClass('count-active');
            setTimeout(function () {
                $('.count-add').removeClass('count-active');;
            }, 1000);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});

$('body').on('click', '.forget-password', function (e) {

    var button = $(this).html();
    showButtonLoader('.forget-password');
    var email = $('.forget-pass-email').val();
    $.ajax({
        url: "/account/forget-password",
        type: "GET",
        data: {email:email},
        success: function (data) {
            hideButtonLoader('.forget-password', button);
            if (data == 1) {
                $(".close").click();
                swal("Уважаемый пользователь.", "Инструкции по сбросу пароля были отправлены на вашу электронную почту.");
            }else{
                swal('Ошибка!',data, 'error');
            }
        },
        error: function () {
            hideButtonLoader('.forget-password', button);
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});







