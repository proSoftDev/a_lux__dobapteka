

$(".review-name").keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});
$(".review-surname").keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});
$('.review-telephone').inputmask("8(999) 999-9999");



$('body').on('click', '#review_button', function (e) {
    var button = $(this).html();
    showButtonLoader('#review_button');
    $.ajax({
        type: 'POST',
        url: '/feedback/add-review',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $("#w0")[0].reset();
                swal('Заявка ушпешно отправлен!', 'В ближайщее время мы свяжемся с вами.', 'success');
            }else{
                swal('Ошибка!', response, 'error');
            }

            hideButtonLoader('#review_button', button);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
            hideButtonLoader('#review_button', button);
        }
    });
});
