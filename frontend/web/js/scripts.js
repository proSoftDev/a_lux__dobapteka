

$(document).ready(function() {
    $('body').on('keyup', '.from_price', function () {

        showLoader();
        //if($(this).val().length >= 3)
        var v_nalichii = 0;
        if ($('.v_nalichii').is(':checked'))
            v_nalichii = 1;
        var akcii = 0;
        if ($('.akcii').is(':checked'))
            akcii = 1;

        $.ajax({
            type: 'GET',
            url: "/catalog/filter",
            data: {
                from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
                v_nalichii: v_nalichii, akcii: akcii
            },
            success: function (html) {
                $('.tablet-version-hide.items').html(html);
                hideLoader();
            }
        });
    });

    $('body').on('keyup', '.to_price', function () {

        showLoader();
        //if($(this).val().length >= 3)
        var v_nalichii = 0;
        if ($('.v_nalichii').is(':checked'))
            v_nalichii = 1;
        var akcii = 0;
        if ($('.akcii').is(':checked'))
            akcii = 1;
        var catalog_id = $(this).attr('data-id');

        $.ajax({
            type: 'GET',
            url: "/catalog/filter",
            data: {
                from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
                v_nalichii: v_nalichii, akcii: akcii, catalog_id: catalog_id
            },
            success: function (html) {
                $('.tablet-version-hide.items').html(html);
                hideLoader();
            }
        });
    });


    $('body').on('click', '.v_nalichii', function () {

        showLoader();
        //if($(this).val().length >= 3)
        var v_nalichii = 0;
        if ($('.v_nalichii').is(':checked'))
            v_nalichii = 1;
        var akcii = 0;
        if ($('.akcii').is(':checked'))
            akcii = 1

        $.ajax({
            type: 'GET',
            url: "/catalog/filter",
            data: {
                from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
                v_nalichii: v_nalichii, akcii: akcii
            },
            success: function (html) {
                $('.tablet-version-hide.items').html(html);
                hideLoader();
            }
        });
    });

    $('body').on('click', '.akcii', function () {

        showLoader();
        //if($(this).val().length >= 3)
        var v_nalichii = 0;
        if ($('.v_nalichii').is(':checked'))
            v_nalichii = 1;
        var akcii = 0;
        if ($('.akcii').is(':checked'))
            akcii = 1

        $.ajax({
            type: 'GET',
            url: "/catalog/filter",
            data: {
                from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
                v_nalichii: v_nalichii, akcii: akcii
            },
            success: function (html) {
                $('.tablet-version-hide.items').html(html);
                hideLoader();
            }
        });
    });

    $('body').on('keyup', '.from_price_mob', function () {

        showLoader();
        //if($(this).val().length >= 3)
        var v_nalichiiMob = 0;
        if ($('.v_nalichiiMob').is(':checked'))
            v_nalichiiMob = 1;
        var akciiMob = 0;
        if ($('.akciiMob').is(':checked'))
            akciiMob = 1;

        var catalog_id = $('.tab-item-active').attr('data-id');
        $.ajax({
            type: 'GET',
            url: "/catalog/filter-mob",
            data: {
                fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob, catalog_id: catalog_id
            },
            success: function (html) {
                $('#home' + catalog_id).html(html);
                hideLoader();
            }
        });
    });

    $('body').on('keyup', '.to_price_mob', function () {
        showLoader();
        var v_nalichiiMob = 0;
        if ($('.v_nalichiiMob').is(':checked'))
            v_nalichiiMob = 1;
        var akciiMob = 0;
        if ($('.akciiMob').is(':checked'))
            akciiMob = 1;
        var catalog_id = $('.tab-item-active').attr('data-id');
        $.ajax({
            type: 'GET',
            url: "/catalog/filter-mob",
            data: {
                fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob, catalog_id: catalog_id
            },
            success: function (html) {
                $('#home' + catalog_id).html(html);
                hideLoader();
            }
        });
    });


    $('body').on('click', '.v_nalichiiMob', function () {
        showLoader();
        var v_nalichiiMob = 0;
        if ($('.v_nalichiiMob').is(':checked'))
            v_nalichiiMob = 1;
        var akciiMob = 0;
        if ($('.akciiMob').is(':checked'))
            akciiMob = 1;
        var catalog_id = $('.tab-item-active').attr('data-id');
        $.ajax({
            type: 'GET',
            url: "/catalog/filter-mob",
            data: {
                fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob, catalog_id: catalog_id
            },
            success: function (html) {
                $('#home' + catalog_id).html(html);
                hideLoader();
            }
        });
    });

    $('body').on('click', '.akciiMob', function () {
        showLoader();
        var v_nalichiiMob = 0;
        if ($('.v_nalichiiMob').is(':checked'))
            v_nalichiiMob = 1;
        var akciiMob = 0;
        if ($('.akciiMob').is(':checked'))
            akciiMob = 1;
        var catalog_id = $('.tab-item-active').attr('data-id');
        $.ajax({
            type: 'GET',
            url: "/catalog/filter-mob",
            data: {
                fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob, catalog_id: catalog_id
            },
            success: function (html) {
                $('#home' + catalog_id).html(html);
                hideLoader();
            }
        });
    });


    $('body').on('click', '.load-more button', function () {
        showLoader();
        var v_nalichii = 0;
        if ($('.v_nalichii').is(':checked'))
            v_nalichii = 1;
        var akcii = 0;
        if ($('.akcii').is(':checked'))
            akcii = 1;

        var category_id = $('.category_id').val();

        $.ajax({
            type: 'GET',
            url: "/catalog/load-more-products",
            data: {
                category_id: category_id,
                from: $('.from_price').val(),
                to: $('.to_price').val(),
                v_nalichii: v_nalichii,
                akcii: akcii
            },
            success: function (html) {
                $('#loadMoreResults').append(html);
                hideLoader();
            }
        });


        $.ajax({
            type: 'GET',
            url: "/catalog/load-more-button",
            data: {
                category_id: category_id,
                from: $('.from_price').val(),
                to: $('.to_price').val(),
                v_nalichii: v_nalichii,
                akcii: akcii
            },
            success: function (html) {
                $('#loadMoreButton').html(html);

            }
        });



    });


    $('body').on('click', '#load-more-mob button', function () {

        showLoader();
        var v_nalichiiMob = 0;
        if ($('.v_nalichiiMob').is(':checked'))
            v_nalichiiMob = 1;
        var akciiMob = 0;
        if ($('.akciiMob').is(':checked'))
            akciiMob = 1;

        var category_id = $('.tab-item-active').attr('data-id');

        $.ajax({
            type: 'GET',
            url: "/catalog/load-more-button-mob",
            data: {
                category_id: category_id, fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob
            },
            success: function (html) {
                hideLoader();
                $('#loadMoreButtonMob').html(html);
            }
        });

        $.ajax({
            type: 'GET',
            url: "/catalog/load-more-products-mob",
            data: {
                category_id: category_id, fromMob: $('.from_price_mob').val(), toMob: $('.to_price_mob').val(),
                v_nalichiiMob: v_nalichiiMob, akciiMob: akciiMob
            },
            success: function (html) {
                $('#home' + category_id + ' #loadMoreResultsMob').append(html);
            }
        });
    });

});
