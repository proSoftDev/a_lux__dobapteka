

<div class="main">
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow slideInLeft">
                    <div class="product-title gradient-text">
                        <h5>Связаться с нами</h5>
                    </div>
                </div>
                <div class="offset-lg-2 col-lg-8 col-sm-12 space-top-sm wow fadeInUp">
                    <form action="/site/add-review" id="w0">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                        <div class="row" >
                            <div class="col-sm-6">

                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/user-icon-light.png" alt="">
                                    </div>
                                    <input class="review-name" type="text" placeholder="Имя" name="Review[name]">
                                </div>
                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/user-icon-light.png" alt="">
                                    </div>
                                    <input class="review-surname" type="text" placeholder="Фамилия" name="Review[surname]">
                                </div>

                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/msg-icon.png" alt="">
                                    </div>
                                    <input type="text" placeholder="Город" name="Review[city]">
                                </div>

                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/phone-icon.png" alt="">
                                    </div>
                                    <input class="review-telephone"  placeholder="Номер телефона" name="Review[telephone]">
                                </div>


                            </div>
                            <div class="col-sm-6">

                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/msg-icon.png" alt="">
                                    </div>
                                    <input type="text" placeholder="Ваш e-mail" name="Review[email]">
                                </div>
                                <div class="review">
                                    <label for="textarea"><img src="/images/review.png" alt="">Сообщение</label>
                                    <textarea id="textarea" name="Review[comment]"></textarea>
                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="review-btn">
                                    <button type="button" id="review_button"  >Отправить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <script src="/js/review.js"></script>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
