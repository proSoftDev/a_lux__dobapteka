<script>
    <? if (Yii::$app->session['modal-pass-reset']) : ?>
        swal("Уважаемый пользователь.", "Ваш старый пароль был сброшен. Новый пароль был отправлен на вашу электронную почту.");
        <? unset(Yii::$app->session['modal-pass-reset']); ?>
    <? endif; ?>

    <? if (Yii::$app->session['guest-order-success']) : ?>
        swal("Спасибо!", "Ваш заказ принят!", "success");
        <? unset(Yii::$app->session['guest-order-success']); ?>
    <? endif; ?>
</script>


<div class="overlay">
    <div class="pick-city-modal">
        <div class="close-modal">
            &#10005
        </div>
        <h2>Выберите ваш город</h2>
        <div class="city-list">

            <? $len = count(Yii::$app->view->params['cities']); ?>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if ($len / 3 >= $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>

            </ul>

            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if ($len / 3 < $m && ($len / 3) * 2 >= $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>
            </ul>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                    <? if (($len / 3) * 2 < $m) : ?>
                        <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                    <? endif; ?>
                    <? $m++; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="content">
    <div class="desktop-version">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="logo wow fadeInLeft">
                            <a href="/">
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="col-sm-2 ">
                        <div class="loc" id="city">
                            <div class="loc-icon">
                                <img src="/images/loc-icon.png" alt="">
                            </div>
                            <span><?= Yii::$app->session['city_name']; ?> <img src="/images/arrow.png" alt=""></span>
                        </div>
                    </div> -->
                    <div class="col-sm-10 d-flex align-items-center">
                        <div class="menu wow fadeInDown">
                            <ul class="m-0">
                                <? foreach (Yii::$app->view->params['menu'] as $v) : ?>
                                    <li><a href="/<?= $v->url; ?>"><?= $v->text; ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div id="myHeader" class="desctop-header-sticky">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="catalog">
                        <a href="/catalog/<?=Yii::$app->view->params['catalog'][0]->url;?>">Каталог</a>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8">
                    <form class="search" method="get" action="/search">
                        <input type="text" id="searchbox" name="text" placeholder="Введите название товара">
                    </form>
                </div>
                <div class="col-sm-1">
                    <div class="user-icon">
                        <? if (!Yii::$app->user->isGuest) : ?>
                            <a href="/account/?tab=bonus">
                                <img src="/images/user-icon.png" alt="">
                            </a>
                            <a href="/account/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        <? endif; ?>
                        <? if (Yii::$app->user->isGuest) : ?>
                            <a href="/account/sign-in">
                                <img src="/images/user-icon.png" alt="">
                            </a>
                        <? endif; ?>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="basket-icon">
                        <a href="/card/">
                            <img src="/images/basket-icon.png" alt="">
                            <span class="count count-add"><?= count($_SESSION['basket']) + count($_SESSION['gift']); ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version header-sticky">
        <header>
            <div class="container">
                <div class="row wow fadeInDown p-1">
                    <div class="col-2" style="display: flex">
                        <nav id="main-nav">
                            <ul>
                                <li><a href="#">Каталог</a>
                                    <ul>
                                        <? foreach (Yii::$app->view->params['catalog'] as $value) : ?>
                                            <li><a href="/catalog/<?= $value->url; ?>"><span class="menu-icon"><img src="/images/menu1.png" alt=""></span>
                                                    <p><?= $value->name; ?></p>
                                                </a>
                                                <ul>
                                                    <? foreach (Yii::$app->view->params['catalogLevel2'] as $v) : ?>
                                                        <? if ($v->parent_id == $value->id) : ?>
                                                            `<li><a href="/catalog/<?= $v->parent->parent->url ? $v->parent->parent->url . '/' : $v->parent->url . '/' ?><?= $v->url ?>"><?= $v->name ?></a>
                                                                <ul>
                                                                    <? foreach (Yii::$app->view->params['catalogLevel3'] as $three) : ?>
                                                                        <? if ($three->parent_id == $v->id) : ?>
                                                                            <li>
                                                                                <a href="/catalog/<?= $three->parent->parent->parent->url ? $three->parent->parent->parent->url . '/' : $three->parent->parent->url . '/' ?><?= $three->parent->url ?>/<?= $three->url ?>"><?= $three->name ?>
                                                                                </a>
                                                                            </li>
                                                                        <? endif; ?>
                                                                    <? endforeach; ?>
                                                                </ul>
                                                            </li>`
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                </ul>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                </li>

                                <? foreach (Yii::$app->view->params['menu'] as $v) : ?>

                                    <li><a href="/<?= $v->url; ?>"><i class="<?=$v->icon;?>"></i><?= $v->text; ?></a></li>
                                    <? if($v->url == "address"):?>
                                        <ul>
                                            <? foreach (Yii::$app->view->params['address'] as $v) : ?>
                                                <li><a href="/site/shops"><?= $v->address; ?></a></li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif;?>
                                <? endforeach; ?>
                            </ul>
                        </nav>

                    </div>
                    <div class="col-7">
                        <div class="logo">
                            <a href="/">
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-3" style="display: flex; justify-content: flex-end; align-items: center; height: 2rem;">
                        <div class="user-icon">
                            <? if (!Yii::$app->user->isGuest) : ?>
                                <a href="/account/?tab=bonus">
                                    <img src="/images/user-icon.png" alt="">
                                </a>
                                <a href="/account/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                            <? endif; ?>
                            <? if (Yii::$app->user->isGuest) : ?>
                                <a href="/account/sign-in">
                                    <img src="/images/user-icon.png" alt="">
                                </a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="col-sm-12 fixed-search">
        <div class="d-flex justify-content-end">
            <form class="search" method="get" action="/search/index">
                <input type="text" id="searchboxMobile" name="text" placeholder="Введите название товара">
            </form>
            <div class="mobile-flex">
                <div class="basket-icon">
                    <a href="/card/">
                        <img src="/images/basket-icon.png" alt="">
                        <span class="count count-add"><?= count($_SESSION['basket']) + count($_SESSION['gift']); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
