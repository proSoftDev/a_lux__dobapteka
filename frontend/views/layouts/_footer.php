<footer class="footer">
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="mobile-version col-sm-12">
                <div class="mobile-phone">
                    <a href="tel:+7 777 352 55 45"><img src="/images/mobile-phone.png" alt=""></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="footer-text">
                    <p><?=Yii::$app->view->params['logo']->copyright;?></p>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="footer-text">
                    <p>Мы в соцсетях: <a href="<?=Yii::$app->view->params['contact']->facebook;?>" target="_blank"><img src="/images/soc1.png" alt="">
                        </a><a href="<?=Yii::$app->view->params['contact']->instagram;?>" target="_blank"><img src="/images/soc2.png" alt=""></a></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer-text">
                    <p><a href="https://www.a-lux.kz/">Разработка сайтов в Алматы</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>



