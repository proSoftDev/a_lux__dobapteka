<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-title text-center gradient-text">
                            <h5>Наши аптеки</h5>
                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <div class="search">
                            <input type="text" placeholder="Поиск" class="filial_search">
                        </div>
                        <div class="checkbox-flex" style="margin: 30px 0;">
                            <label class="filter-checkbox">Открыто
                                <input type="checkbox" id="checkbox_desktop">
                                <span class="checkmark"></span>
                            </label>

                        </div>
                        <div class="map">
                            <div id="map" style="width: 100%; height: 480px;border:0" ></div>
                         
                            <script>
                                //document.getElementById("office-city").innerHTML="<?//=$city->name?>//";
                                ymaps.ready(init);
                                var center_map = [0, 0];
                                var map = "";
                                function init() {
                                    map = new ymaps.Map('map', {
                                        center: center_map,
                                        zoom: 11,
                                    });

                                    var myGeocoder = ymaps.geocode("<?=Yii::$app->session["city_name"];?>");
                                    myGeocoder.then(
                                        function (res) {
                                            var street = res.geoObjects.get(0);
                                            var coords = street.geometry.getCoordinates();
                                            map.setCenter(coords);
                                        },
                                        function (err) {

                                        }
                                    );

                                    <?foreach($filial as $v){?>
                                        map.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                            balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                                        }, {
                                            iconLayout: 'default#image',
                                            iconImageHref: '/images/loc-icon.png',
                                            preset: 'islands#icon',
                                            iconColor: '#0095b6'
                                        }));
                                    <?}?>


                                    $('body').on('change', '#checkbox_desktop',function(e){
                                        e.preventDefault();
                                        map.geoObjects.removeAll();
                                        var checked = $("#checkbox_desktop:checked").length;
                                        if (checked != 0) {
                                            $.ajax({
                                                type: "GET",
                                                url: "/address/get-open-shops",
                                                success: function (response) {
                                                    $('#filial').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                url: "/address/get-open-map-shops",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        map.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });


                                        }else {
                                            $.ajax({
                                                type: "GET",
                                                url: "/address/get-close-shops",
                                                success: function (response) {
                                                    $('#filial').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                url: "/address/get-close-map-shops",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        map.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });
                                        }
                                    });

                                    $('.filial_search').on("keypress",function(e){
                                        if(e.which === 13){
                                            e.preventDefault();
                                            map.geoObjects.removeAll();
                                            text = $(this).val();
                                            $(this).val("");
                                            $.ajax({
                                                type: "GET",
                                                data: {text: text},
                                                url: "/address/get-search-result",
                                                success: function (response) {
                                                    $('#filial').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                data: {text: text},
                                                url: "/address/get-search-map-result",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        map.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });
                                        }
                                    });
                                }

                            </script>
                        </div>
                        <div class="row" id="filial">
                            <?foreach($filial as $v):?>
                            <div class="col-sm-6">
                                <div class="drugstore-wrap">
                                    <div class="drugstore-item">
                                        <p><img src="/images/loc-icon.png" alt=""><b><?=$v->address;?></b></p>
                                        <? try {
                                            $from_time = new DateTime($v->from_time);
                                            $to_time = new DateTime($v->to_time);
                                        } catch (Exception $e) {
                                        } ?>
                                        <p><img src="/images/clock.png" alt=""><?=$from_time->format('H:i');?> - <?=$to_time->format('H:i');?> ч.</p>
                                        <p><img src="/images/phone-icon.png" alt=""><?=$v->telephone;?> </p>
                                        <? if($v->currentStatus == 'Открыто'):?>
                                            <div class="status-open">
                                                <?=$v->currentStatus;?>
                                            </div>
                                        <? endif?>
                                        <? if($v->currentStatus == 'Закрыто'):?>
                                            <div class="status-close">
                                                <?=$v->currentStatus;?>
                                            </div>
                                        <? endif?>
                                    </div>
                                </div>
                            </div>
                            <? endforeach;?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="register-title text-center gradient-text">
                            <h5>Наши аптеки</h5>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="search">
                            <input type="text" placeholder="Поиск" class="filial_search_mob">
                        </div>
                        <div class="checkbox-flex">
                            <label class="filter-checkbox">Открыто
                                <input type="checkbox" id="checkbox_mobile">
                                <span class="checkmark"></span>
                            </label>

                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <div class="map">
                            <div id="mapMob" style="width: 100%; height: 480px;border:0" ></div>
                            <script>

                                ymaps.ready(init);
                                var center_map = [0, 0];
                                var mapMob = "";
                                function init() {
                                    mapMob = new ymaps.Map('mapMob', {
                                        center: center_map,
                                        zoom: 11,
                                    });

                                    var myGeocoder = ymaps.geocode("<?=Yii::$app->session['city_name'];?>");
                                    myGeocoder.then(
                                        function (res) {
                                            var street = res.geoObjects.get(0);
                                            var coords = street.geometry.getCoordinates();
                                            mapMob.setCenter(coords);
                                        },
                                        function (err) {

                                        }
                                    );

                                    <?foreach($filial as $v){?>
                                        mapMob.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                            balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                                        }, {
                                            iconLayout: 'default#image',
                                            iconImageHref: '/images/loc-icon.png',
                                            preset: 'islands#icon',
                                            iconColor: '#0095b6'
                                        }));
                                    <?}?>

                                    $('body').on('change', '#checkbox_mobile',function(e){
                                        e.preventDefault();
                                        mapMob.geoObjects.removeAll();
                                        var checked = $("#checkbox_mobile:checked").length;
                                        if (checked != 0) {
                                            $.ajax({
                                                type: "GET",
                                                url: "/address/get-open-shops",
                                                success: function (response) {
                                                    $('#filial_mob').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                url: "/address/get-open-map-shops",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        mapMob.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });


                                        }else {
                                            $.ajax({
                                                type: "GET",
                                                url: "/address/get-close-shops",
                                                success: function (response) {
                                                    $('#filial_mob').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                url: "/address/get-close-map-shops",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        mapMob.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });
                                        }
                                    });


                                    $('.filial_search_mob').on("keypress",function(e){
                                        if(e.which === 13){
                                            e.preventDefault();
                                            mapMob.geoObjects.removeAll();
                                            text = $(this).val();
                                            $(this).val("");
                                            $.ajax({
                                                type: "GET",
                                                data: {text: text},
                                                url: "/address/get-search-result",
                                                success: function (response) {
                                                    $('#filial_mob').html(response);
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });

                                            $.ajax({
                                                type: "GET",
                                                dataType: "json",
                                                data: {text: text},
                                                url: "/address/get-search-map-result",
                                                success: function (data) {
                                                    for (i = 0; i < data.length; i++) {
                                                        latitude = data[i][0];
                                                        longitude = data[i][1];
                                                        address = data[i][2];
                                                        telephone = data[i][3];

                                                        mapMob.geoObjects.add(new ymaps.Placemark([latitude, longitude], {
                                                            balloonContent: '<div class="company-name  fadeInUp">' + address + '</br>'  + telephone + '</div>'
                                                        }, {
                                                            iconLayout: 'default#image',
                                                            iconImageHref: '/images/loc-icon.png',
                                                            preset: 'islands#icon',
                                                            iconColor: '#0095b6'
                                                        }));
                                                    }
                                                },
                                                error: function () {
                                                    swal('Упс!', 'Что-то пошло не так.', 'error');
                                                }
                                            });
                                        }
                                    });
                                }
                            </script>
                        </div>
                        <div class="row" id="filial_mob">
                            <?foreach($filial as $v):?>
                                <div class="col-sm-6">
                                    <div class="drugstore-wrap">
                                        <div class="drugstore-item">
                                            <p><img src="/images/loc-icon.png" alt=""><b><?=$v->address;?></b></p>
                                            <? try {
                                                $from_time = new DateTime($v->from_time);
                                                $to_time = new DateTime($v->to_time);
                                            } catch (Exception $e) {
                                            } ?>
                                            <p><img src="/images/clock.png" alt=""><?=$from_time->format('H:i');?> - <?=$to_time->format('H:i');?> ч.</p>
                                            <p><img src="/images/phone-icon.png" alt=""><?=$v->telephone;?> </p>
                                            <? if($v->currentStatus == 'Открыто'):?>
                                                <div class="status-open">
                                                    <?=$v->currentStatus;?>
                                                </div>
                                            <? endif?>
                                            <? if($v->currentStatus == 'Закрыто'):?>
                                                <div class="status-close">
                                                    <?=$v->currentStatus;?>
                                                </div>
                                            <? endif?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
