<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = "Регистрация";
?>

<div class="main">
    <div class="desktop-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="register-title text-center gradient-text wow fadeInDown">
                            <h5>Регистрация</h5>
                        </div>
                        <div class="register-by  gradient-text wow fadeInDown">
                            <p>Регистрация c помощью:</p>
                            <?= yii\authclient\widgets\AuthChoice::widget([
                                'baseAuthUrl' => ['account/auth'],
                                'popupMode' => false,
                            ]) ?>
                        </div>
                    </div>
                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <?php $form = ActiveForm::begin(); ?>

                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="ФИО" name="UserProfile[fio]">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/msg-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Почта" name="SignupForm[email]">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/phone-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Номер" name="SignupForm[username]" class="telephone">
                                        <script>
                                            $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                                        </script>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/loc-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Адрес" name="UserAddress[address][]">
                                    </div>
                                    <div class="field_wrapper">
                                        <div>

                                        </div>
                                        <div class="add-address gradient-text">
                                            <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="capcha-img">{image}<br /><div id="refresh-captcha" class="btn btn-ticked add_sl_per">Обновить</div></div>',
                                        'imageOptions' => [
                                            'id' => 'my-captcha-image'
                                        ]
                                    ])->label(false)?>

                                    <?php $this->registerJs("
                                        $('body').on('click', '#refresh-captcha', function(e){
                                            e.preventDefault();
                                            $('#my-captcha-image').yiiCaptcha('refresh');
                                        });
                                    "); ?>

                                    <div class="personal-input">
                                        <input type="text" placeholder="Введите код с картинки" name="SignupForm[verifyCode]">
                                    </div>
                                    <div class="review-btn">
                                        <button type="button" class="register-button">Зарегистрироваться</button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInput" placeholder="Пароль" name="SignupForm[password]">
                                        <button type="button" onclick="showPass()"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInput2" placeholder="Повторить пароль" name="SignupForm[password_verify]">
                                        <button type="button" onclick="showPass2()"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="register-bonus">
                                        <p>Начисление бонусов при регистрации</p>
                                        <p><span class="gradient-text">500</span> тг!</p>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInDown">
                        <div class="register-title text-center gradient-text">
                            <h5>Регистрация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Регистрация c помощью:  </p>
                            <div class="row">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['account/auth'],
                                    'popupMode' => false,
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="offset-lg-2 col-lg-8 col-sm-12 wow fadeInUp">
                        <?php $form = ActiveForm::begin(); ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Фамилия" name="UserProfile[surname]">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="" >
                                        </div>
                                        <input type="text" placeholder="Имя" name="UserProfile[name]">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/user-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Отчество" name="UserProfile[father]">
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/msg-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Почта" name="SignupForm[email]">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/phone-icon-light.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Номер" name="SignupForm[username]" class="telephone-mobile">
                                        <script>
                                            $('input[name="SignupForm[username]"]').inputmask("8(999) 999-9999");
                                        </script>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/loc-icon.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Адрес" name="UserAddress[address][]">
                                    </div>
                                    <div class="field_wrapper">
                                        <div>

                                        </div>
                                        <div class="add-address gradient-text">
                                            <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password" id="myInputm" placeholder="Пароль" name="SignupForm[password]">
                                        <button  onclick="showPassm()" type="button"><img src="/images/show-icon.png" alt=""></button>
                                    </div>
                                    <div class="personal-input">
                                        <div class="icon">
                                            <img src="/images/lock-icon.png" alt="">
                                        </div>
                                        <input type="password"  id="myInput2m" placeholder="Повторить пароль" name="SignupForm[password_verify]">
                                        <button onclick="showPassm2()" type="button"><img src="/images/show-icon.png" alt=""></button>
                                    </div>

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="capcha">{image}<br />
                                                            <div id="refresh-captcha-mobile" class="btn btn-ticked add_sl_per">Обновить
                                                            </div>
                                                        </div>
                                                        <div class="personal-input">
                                                            <div class="icon">
                                                                <img src="/images/lock-icon.png" alt="">
                                                            </div>
                                                            {input}
                                                        </div>',
                                        'imageOptions' => [
                                            'id' => 'my-captcha-image-mobile'
                                        ]
                                    ])->label(false)?>

                                    <?php $this->registerJs("
                                        $('body').on('click', '#refresh-captcha-mobile', function(e){
                                            e.preventDefault();
                                            $('#my-captcha-image-mobile').yiiCaptcha('refresh');
                                        });
                                    "); ?>

                                    <div class="review-btn">
                                        <button type="button" class="register-button-mob">Зарегистрироваться</button>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="register-bonus">
                                        <p>Начисление бонусов при регистрации</p>
                                        <p><span class="gradient-text">500</span> тг!</p>
                                    </div>
                                </div>

                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
