<div class="row">
    <?if($fav == null):?>
        <div >
            <p>В избранном отсутствуют товары.</p>
        </div>
    <? endif;?>
    <?if($fav != null):?>
        <? foreach ($fav as $v):?>
            <div class="col-sm-4">
                <div class="catalog-item">
                    <div class="image">
                        <?$img = unserialize($v->images);$img = $img[0]?>
                        <a href="/product/<?=$v->url?>">
                            <img src="/backend/web/<?=$v->path.$img?>" alt="">
                        </a>
                    </div>
                    <div class="text-block">
                        <div class="delete-icon btn-delete-product-from-favorite"  data-id="<?=$v->id;?>">
                            &#10005
                        </div>
                        <div class="name">
                            <p><?=$v->name?></p>
                        </div>
                        <div class="status">
                            <p><img src="/images/ok.png" alt=""><?if($v->status==1){?>Есть<?}else{?>Нет<?}?>
                                в наличии</p>
                        </div>
                        <div class="flex-one">
                            <div class="price">
                                <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                            </div>
                            <div class="sale-basket-button">
                                <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach;?>
    <? endif;?>
</div>
