<div class="order">
    <div class="container">
        <div class="row">
			<? foreach($products as $product):?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="order-content">
                        <h4><?=$product->product->name;?></h4>
                        <br>
                        <p>Цена: <span><?=$product->product->getCalculatePrice();?>тг</span></p>
                        <hr>
                        <p>Количество: <span><?=$product->count;?></span></p>
                        <hr>
                        <p>Общая цена: <span><?=$product->count*$product->product->getCalculatePrice();?>тг</span></p>
                        <hr>
                        <a href="/product/<?=$product->product->url;?>" class="btn-order">Подробнее</a>
                    </div>
                </div>
			<? endforeach;?>
            <? foreach($gifts as $product):?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="order-content">
                        <h4><?=$product->product->name;?></h4>
                        <br>
                        <p style="position: center;">Подарок</p>
                        <hr>
                        <p>Количество: <span><?=$product->count;?></span></p>
                        <hr>
                        <a href="/product/<?=$product->product->url;?>" class="btn-order">Подробнее</a>
                    </div>
                </div>
            <? endforeach;?>
        </div>
    </div>
</div>
