<div class="delivery-cost">
    <a href="#" data-toggle="modal" data-target="#exampleModal">Укажите адрес доставки на карте</a>
</div>

<div class="delivery-cost">
    <p>Адрес доставки:</p>
    <div class="price">
        <p class="gradient-text" id="deliveryAddress"></p>
    </div>
</div>
<input type="hidden" id="nearlyAddress">

<!-- <div id="map" style="width: 100%; height: 300px; float:left;"></div> -->
<div id="viewContainer"></div>
<div class="modal modal-map fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="w-50 adress-title">
                    <h5 class="modal-title" id="exampleModalLabel">Адрес доставки</h5>
                    <div class="search-map">
                        <form>
                            <!-- <span deliveryAddressclass="delete" onclick="deleteBasketSearchText()"> &#10005</span> -->
                            <input type="text" id="addressSearch" placeholder="Например: ул. Макатаева, д. 28, стр. 1" class="deliveryAddressInput" >
                        </form>
                        <button class="btn-map" id="btnSaveDelivery" onclick="saveAddress()">Хорошо</button>
                        <button class="btn-map-disabled" id="btnSaveDeliveryDisabled" disabled>Хорошо</button>

                    </div>
                </div>
                <div class="delivery-modal">

                    <div class="delivery-cost">
                        <p>Сумма за доставку:</p>
                        <div class="price">
                            <p class="gradient-text deliveryPrice"></p>
                        </div>
                    </div>
                    <div class="delivery-cost">
                        <p>Время доставки:</p>
                        <div class="price">
                            <p class="gradient-text deliveryTime"></p>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body p-0">
                <div id="map" style="width: 100%; height: 85vh; float:left;"></div>
            </div>
        </div>
    </div>
</div>
<script>
    ymaps.ready(init);

    function init() {
        var myPlacemark, myMap = new ymaps.Map("map", {
            center: [43.238293, 76.945465],
            zoom: 11,
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });

        var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
        myGeocoder.then(
            function(res) {
                var street = res.geoObjects.get(0);
                var coords = street.geometry.getCoordinates();
                myMap.setCenter(coords);
            },
            function(err) {

            }
        );


        $("#addressSearch").autocomplete({
            source: function(request, response) {
                hideSaveButton();

                var address = request.term;
                var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>, " + address);
                myGeocoder.then(
                    function(res) {
                        var coords = res.geoObjects.get(0).geometry.getCoordinates();
                        var myGeocoder = ymaps.geocode(coords, {
                            kind: 'house'
                        });
                        myGeocoder.then(
                            function(res) {
                                var myGeoObjects = res.geoObjects;
                                var addresses = [];
                                myGeoObjects.each(function(el, i) {
                                    var arr = [];
                                    arr['title'] = el.properties.get('name');
                                    arr['value'] = 'Казахстан, <?= Yii::$app->session["city_name"]; ?> , ' + el.properties.get('name');
                                    arr['coords'] = el.geometry.getCoordinates();
                                    addresses.push(arr);
                                });

                                response($.map(addresses, function(address) {
                                    return {
                                        label: address.title,
                                        value: address.value,
                                        coords: address.coords
                                    }
                                }));
                            }
                        );
                    },
                    function(err) {
                        alert('Ошибка');
                    }
                );

            },
            minLength: 2,
            mustMatch: true,
            select: function(event, ui) {
                $('#addressSearch').val(ui.item.value);

                // --- проверка точки кординаты внутри ли адрес доставки
                var inPolygon = false;
                <? foreach ($model as $v) { ?>
                    <? $array = unserialize($v->coords); ?>
                    var coords = [];
                    <? foreach ($array[0] as $arr) { ?>
                        coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
                    <? } ?>

                    var myPolygon = new ymaps.Polygon([coords]);
                    myPolygon.options.setParent(myMap.options);
                    myPolygon.geometry.setMap(myMap);
                    if (myPolygon.geometry.contains(ui.item.coords)) {
                        inPolygon = true;
                        <? $sum = $v->summ; ?>
                        <? $time = $v->name; ?>
                    }
                <? } ?>
                // ---

                if (inPolygon) {
                    // if selected address in current polygon
                    myMap.geoObjects.remove(myColumn);
                    saveNearlyAddress(ui.item.coords);

                    $.ajax({
                        url: "/card/update-session",
                        type: "GET",
                        data: {
                            deliveryPrice: <?= $sum ?>,
                            deliveryTime: <?= $time; ?>
                        },
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 1) {
                                $('.deliveryPrice').html(data.deliveryPrice + " <span>тг.</span>");
                                $('.deliveryTime').html("<?= $time; ?> <span>минут</span>");
                                $('#sumBasket').html(data.sum + " <span>тг.</span>");
                            }
                        },
                        error: function() {
                            swal('Упс!', 'Что-то пошло не так.', 'error');
                        }
                    });

                    placemark(myMap, ui.item.coords);
                    showSaveButton();
                } else {
                    swal('', 'К сожалению, доставка в данной черте города не осуществляется', 'error');
                }

                return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });


        <? foreach ($model as $v) { ?>
            <? $array = unserialize($v->coords); ?>
            var coords = [];
            <? foreach ($array[0] as $arr) { ?>
                coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
            <? } ?>
            var myPolygon = new ymaps.Polygon([coords], {
                // Описываем свойства геообъекта.
                // Содержимое балуна.
                hintContent: "Адрес доставки"
            }, {
                // Задаем опции геообъекта.
                // Цвет заливки.
                fillColor: '#00FF0088',
            });

            myMap.geoObjects.add(myPolygon);
            var myColumn;
            myPolygon.events.add('click', function(e) {
                myMap.geoObjects.remove(myColumn);
                var coords = e.get('coords');
                saveNearlyAddress(coords);

                $.ajax({
                    url: "/card/update-session",
                    type: "GET",
                    data: {
                        deliveryPrice: <?= $v->summ ?>,
                        deliveryTime: <?= $v->name; ?>
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 1) {
                            $('.deliveryPrice').html(data.deliveryPrice + " <span>тг.</span>");
                            $('.deliveryTime').html("<?= $v->name; ?> <span>минут</span>");
                            $('#sumBasket').html(data.sum + " <span>тг.</span>");
                        }
                    },
                    error: function() {
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });

                placemark(myMap, coords);
                showSaveButton();

            });
        <? } ?>

        function placemark(myMap, coords) {
            myColumn = new ymaps.Placemark(coords, {
                balloonContent: ''
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6',
                strokeWidth: 0
            });
            myMap.geoObjects.add(myColumn);

            // Если метка уже создана – просто передвигаем ее.
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function() {
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords);
        }


        // Создание метки.
        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconCaption: 'поиск...'
            }, {
                preset: 'islands#violetDotIconWithCaption',
                draggable: true
            });
        }
        // Определяем адрес по координатам (обратное геокодирование).
        function getAddress(coords) {
            myPlacemark.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function(res) {
                var firstGeoObject = res.geoObjects.get(0);

                myPlacemark.properties
                    .set({
                        // Формируем строку с данными об объекте.
                        iconCaption: [
                            // Название населенного пункта или вышестоящее административно-территориальное образование.
                            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                        ].filter(Boolean).join(', '),
                        // В качестве контента балуна задаем строку с адресом объекта.
                        balloonContent: firstGeoObject.getAddressLine()
                    });
                $('.deliveryAddress').html(firstGeoObject.getAddressLine());
                $('.deliveryAddressInput').val(firstGeoObject.getAddressLine());
            });
        }



        function saveNearlyAddress(coords) {
            // START NEARLY ADDRESS
            var address = [];
            var distance = [];
            <? foreach ($address as $val) : ?>
                var dist = ymaps.coordSystem.geo.getDistance([coords[0].toPrecision(9), coords[1].toPrecision(9)], [<?= $val->latitude; ?>, <?= $val->longitude; ?>]);
                distance.push(dist);
                address.push([dist, "<?= $val->address; ?>"]);
            <? endforeach; ?>

            var nearlyAddress;
            for (var i = 0; i < address.length; i++) {
                if (address[i][0] == Math.min.apply(Math, distance)) {
                    nearlyAddress = address[i][1];
                    break;
                }
            }
            $('#nearlyAddress').val(nearlyAddress);
            // END  NEARLY ADDRESS
        }


        function showSaveButton() {
            $('#btnSaveDeliveryDisabled').hide();
            $('#btnSaveDelivery').show();
        }



        function hideSaveButton() {
            $('#btnSaveDeliveryDisabled').show();
            $('#btnSaveDelivery').hide();
            $('.deliveryPrice').html("");
            $('.deliveryTime').html("");
        }




    }


    function saveAddress() {
        var address = $('.deliveryAddressInput').val();
        $('#deliveryAddress').html(address);
        $(".close").click();
    }


    function deleteBasketSearchText() {
        $('.deliveryAddressInput').val("");
        $('#deliveryAddress').html("");
        $('#btnSaveDeliveryDisabled').show();
        $('#btnSaveDelivery').hide();
        $('.deliveryPrice').html("");
        $('.deliveryTime').html("");
    }


</script>
