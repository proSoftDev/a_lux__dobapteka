<div class="col-sm-12">
    <div class="popup-title">
        <h6> Вы получили подарок! Выберите любой <br> доступный</h6>
    </div>
</div>
<? if($type == 1) $class = 'gift_for_price';?>
<? if($type == 0) $class = 'gift_for_product';?>
<? foreach ($gifts as $v):?>
    <div class="col-sm-3 <?=$class;?>" data-id="<?=$v->id;?>" data-name="<?=$v->name;?>" >
        <div class="gift-item">
            <?$img = unserialize($v->images);$img = $img[0]?>
            <img src="/backend/web/<?=$v->path.$img?>" alt="">
            <p><?=$v->name;?></p>
            <? if($v->id == $id):?>
                <div class="checked">
                    <img src="/images/ok.png" alt="">
                </div>
            <? endif;?>
        </div>
    </div>
<? endforeach;?>
<div class="col-sm-12">
    <div class="bonus-btn">
        <button style="display:block;margin: 40px auto;width: 200px; height: 50px;" id="save_gift">Выбрать</button>
    </div>
</div>