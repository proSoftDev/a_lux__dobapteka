<div class="main">
    <div class="tab-line">
        <div class="container">
            <div class="basket-title wow slideInLeft">
                <h5 class="gradient-text">Корзина</h5>
                <div class="count">
                    <?= count($_SESSION['basket']); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-lg-12">
                    <div class="row ">
                        <?php if ((count($_SESSION['basket']) + count($_SESSION['gift'])) == 0) : ?>
                            <div>
                                <p>В вашей корзине пусто :(</p>
                            </div>
                        <?php endif; ?>
                        <?php if (count($_SESSION['basket']) != 0 || count($_SESSION['gift']) != 0) : ?>

                            <? foreach ($_SESSION['basket'] as $v) : ?>
                                <div class="col-xl-4 col-md-6 wow fadeInUp">
                                    <div class="basket-item">
                                        <div class="image">
                                            <? $img = unserialize($v->images);
                                            $img = $img[0] ?>
                                            <img src="/backend/web/<?= $v->path . $img ?>" alt="">
                                        </div>
                                        <div class="text-block">
                                            <div class="delete-icon btn-delete-product-from-basket" data-id="<?= $v->id ?>">
                                                &#10005
                                            </div>
                                            <div class="name">
                                                <a href="/product/<?= $v->url ?>"><?= $v->name ?></a href="#">
                                            </div>

                                            <div class="flex-one">
                                                <div class="status">
                                                    <? if ($v->status) : ?>
                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                    <? endif; ?>
                                                    <? if (!$v->status) : ?>
                                                        <p>Нет в наличии</p>
                                                    <? endif; ?>
                                                </div>
                                                <div class="count-input">
                                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus" data-id="<?= $v->id ?>">-</span>
                                                    <input class="quantity input-text" min="1" value="<?= $v->count ?>" data-id="<?= $v->id ?>" id="countProduct<?= $v->id ?>" type="number">
                                                    <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus" data-id="<?= $v->id ?>">+</span>
                                                </div>
                                                <div class="price">
                                                    <p class="gradient-text" id="sumProduct<?= $v->id ?>"><?= $v->getSumProduct($v->id); ?> <span>тг.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>


                            <? foreach ($_SESSION['gift'] as $v) : ?>
                                <div class="col-sm-4  <?= $v->count ?>">
                                    <div class="basket-item">
                                        <div class="image">
                                            <? $img = unserialize($v->images);
                                            $img = $img[0] ?>
                                            <img src="/backend/web/<?= $v->path . $img ?>" alt="">
                                        </div>
                                        <div class="text-block">
                                            <div class="delete-icon btn-delete-gift-from-basket" data-id="<?= $v->id ?>">
                                                &#10005
                                            </div>
                                            <div class="name">
                                                <a href="#"><?= $v->name ?></a href="#">
                                            </div>
                                            <div class="flex-one">
                                                <div class="status">
                                                    <? if ($v->status) : ?>
                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                    <? endif; ?>
                                                    <? if (!$v->status) : ?>
                                                        <p>Нет в наличии</p>
                                                    <? endif; ?>
                                                </div>

                                                <div class="count-input">
                                                    <input class="quantity input-text" value="<?=$v->count;?>" disabled>
                                                </div>

                                                <div class="price">
                                                        <p class="gradient-text">Бесплатно</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>

                            <div class="col-sm-12 wow fadeInUp">
                                <div class="basket-title ">
                                    <h6>Просчета доставки</h6>
                                </div>
                            </div>
                            <div class="col-sm-12"></div>
                            <div class="col-sm-4 wow fadeInUp">
                                <div class="basket-select">
                                    <select id="typePay" >
                                        <option value="0">Выбор оплаты</option>
                                        <option value="1" <?=Yii::$app->session['payment_choice'] == 1 ? "selected":"";?>>Наличными курьеру</option>
                                        <option value="2" <?=Yii::$app->session['payment_choice'] == 2 ? "selected":"";?>>Онлайн оплата</option>
                                    </select>
                                </div>

                                <div class="sdacha" style="<?=Yii::$app->session['payment_choice'] == 1 ? '':'display: none';?>">
                                    <p>Подготовить сдачу с суммы</p>
                                    <div class="d-flex">
                                        <input type="number" id="change"><span class="gradient-text">тг</span>
                                    </div>
                                </div>
                                <div class="basket-select">
                                    <select id="typeDelivery">
                                        <option value="0">Выбор доставки</option>
                                        <option value="1" <?=Yii::$app->session['delivery_choice'] == 1 ? "selected":"";?>>Курьером</option>
                                        <option value="2" <?=Yii::$app->session['delivery_choice'] == 2 ? "selected":"";?>>Самовывоз</option>
                                    </select>
                                </div>

                                <? if(Yii::$app->session['delivery_choice'] == 2 || Yii::$app->session['delivery_choice'] == 1):?>
                                    <div class="col-sm-12 wow fadeInUp" id="deliveryInform" style="<?=Yii::$app->session['delivery_choice'] == 2 ? 'display: none':'';?>" >
                                        <?= $this->render('deliveryMap', compact('model', 'address')); ?>
                                    </div>
                                    <div class="col-sm-12 wow fadeInUp" id="addressInform"  style="<?=Yii::$app->session['delivery_choice'] == 1 ? 'display: none':'';?>">
                                        <?= $this->render('deliveryAddress', compact('model', 'address')); ?>
                                    </div>
                                <? else:?>
                                    <div class="col-sm-12 wow fadeInUp" id="deliveryInform" style="display: none" >
                                        <?= $this->render('deliveryMap', compact('model', 'address')); ?>
                                    </div>
                                    <div class="col-sm-12 wow fadeInUp" id="addressInform"  style="display: none">
                                        <?= $this->render('deliveryAddress', compact('model', 'address')); ?>
                                    </div>
                                <? endif;?>

                            </div>

                            <div class="col-sm-4 wow fadeInUp">

                                <? if (!Yii::$app->user->isGuest) : ?>
                                    <div class="personal-input" id="promo_code">
                                        <div class="icon">
                                            <img src="/images/advantage-icon2.png" alt="">
                                        </div>
                                        <input type="text" placeholder="Промокод" class="promocode">
                                        <button class="green-btn">Проверить</button>
                                    </div>
                                <? endif; ?>
                                <div>
                                    <? if (!Yii::$app->user->isGuest) : ?>
                                        <div class="bonus-cost opacity-bonus">
                                            <p>Оплата бонусами</p>
                                            <div class="price">
                                                <p>
                                                    <?= $bonus->sum; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <div class="total-cost">
                                        <p>Итого:</p>
                                        <div class="price">
                                            <p class="gradient-text" id="sumBasket"><?= intval($sum * (100 - $_SESSION['earliest']) / 100); ?> <span>тг.</span></p>
                                        </div>

                                    </div>
                                    <div class="total-cost">
                                        <p><?= $text[0]->text; ?></p>
                                        <div class="price">
                                            <p class="gradient-text" id="percentPromo"> <?= $_SESSION['promo']; ?><span>%</span></p>
                                        </div>
                                    </div>
                                    <? if (!Yii::$app->user->isGuest) : ?>
                                        <div class="total-cost">
                                            <p><?= $text[1]->text; ?> <span class="gradient-text"><?= $text[2]->text; ?></span></p>
                                            <div class="price">
                                                <p class="gradient-text"><?= $_SESSION['earliest']; ?><span>%</span></p>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <br>
                                <div class="pay-btn">
                                    <button id="button-pay" data-user="<?=Yii::$app->user->isGuest ?>"><?=Yii::$app->session['payment_choice'] == 1 ? "Заказать":"Оплатить";?></button>
                                </div>
                            </div>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
