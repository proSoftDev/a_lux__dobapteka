<input type="hidden" value="<?=$id?>" class="category_id">
<div class="row" id="loadMoreResults">
    <? $m = 0; ?>
    <? foreach ($products as $v) { ?>
        <? $m++; ?>
        <? if ($m > $per_page) break; ?>
        <input type="hidden" value="<?= $v->category_id ?>" class="category_id">
        <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="catalog-item">
                <div class="image">
                    <? $img = unserialize($v->images);
                    $img = $img[0] ?>
                    <a href="/product/<?= $v->url ?>">
                        <img src="/backend/web/<?= $v->path . $img ?>" alt="">
                    </a>
                </div>
                <div class="text-block">
                    <? if ($v->status_products) { ?>
                        <div class="red-label">
                            <?= $v->status_products ?>
                        </div>
                    <? } ?>
                    <div class="name">
                        <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                            <p><?=$v->name?></p>
                        </a>
                    </div>
                    <div class="status">
                        <? if ($v->status) : ?>
                            <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                        <? endif; ?>
                        <? if (!$v->status) : ?>
                            <p>Нет в наличии</p>
                        <? endif; ?>
                        <? if(Yii::$app->user->isGuest):?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                        <?else:?><i class="fas fa-heart like-active favorites <?=$v->getFavoriteStatus() ? 'favorites-active':'';?>"  data-id="<?=$v->id;?>" ></i>
                        <? endif;?>
                    </div>
                    <div class="flex-one">
                        <div class="price">
                            <p class="gradient-text"><?= $v->calculatePrice; ?><span>тг.</span></p>
                        </div>
                        <div class="catalog-basket-button">
                            <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalog"><img src="/images/light-basket.png" alt="">В корзину</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</div>
<div class="row" id="loadMoreButton">
    <? if (count($products) > $per_page) : ?>
        <div class="col-sm-12">
            <div class="collapse-wrap">
                <div class="load-more">
                    <button>Загрузить еще</button>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>
<div class="row">
    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalog"></div>
    <?= $this->render("_bonus_web"); ?>
</div>
