<? if($sop_tovary):?>
<div class="product-title gradient-text border-top">
    <h5>Сопутствующие товары</h5>
</div>
<div class="owl2 owl-carousel owl-theme">
    <? foreach ($sop_tovary as $v):?>
        <? $product = $v->product;?>
        <div class="item">
            <div class="top-sale-item">
                <?$img = unserialize($product->images);$img = $img[0];?>
                <a href="/product/<?=$product->url;?>"><img src="/backend/web/<?=$product->path.$img;?>" alt=""></a>
                <div class="name">
                    <a href="/product/<?=$product->url;?>"><p><?=$product->name;?></p></a>
                </div>
                <div class="price">
                    <p class="gradient-text"><?=$product->calculatePrice;?> <span>тг.</span></p>
                </div>
                <div class="basket-button">
                    <button class="btn-in-basket" data-id="<?=$product->id;?>"><img src="images/light-basket.png" alt="">В корзину</button>
                </div>
            </div>
        </div>
    <? endforeach;?>
</div>
<? endif;?>
