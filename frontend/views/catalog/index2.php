<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft">
                        <div class="catalog-menu">
                            <div class="title gradient-text">
                                Каталог товаров
                            </div>
                            <?=$result?>
                        </div>
                        <div class="tablet-version-hide">
                            <div class="main-title gradient-text">
                                <h5>Товар месяца</h5>
                            </div>
                            <?foreach($top_products as $v){?>
                                <div class="month-sale-item">
                                    <a href="">
                                        <div class="top">
                                            <div class="image">
                                                <?$img = unserialize($v->images);$img = $img[0]?>
                                                <a href="/product/<?=$v->url?>">
                                                    <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                                </a>
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                        <p><?=$v->name?></p>
                                                    </a>
                                                </div>
                                                <div class="price">
                                                    <?if($v->discount){echo '<p>'.$v->price.'</p>';}?>
                                                    <p class="gradient-text"> <?=$v->calculatePrice?><span>тг</span></p>
                                                </div>
                                                <div class="status">
                                                    <? if($v->status):?>
                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                    <? endif;?>
                                                    <? if(!$v->status):?>
                                                        <p>Нет в наличии</p>
                                                    <? endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bot">
                                            <div class="sale">
                                                <?if($v->discount):?>
                                                    <p><?=$v->discount?>%</p>
                                                <? endif;?>
                                                <?if($v->bonus):?>
                                                    <div class="bonus">
                                                        <?=$v->bonus?> Бонус
                                                    </div>
                                                <? endif;?>
                                            </div>
                                            <div class="sale-basket-button">
                                                <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?}?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-6 col-sm-12">
                        <div class="white-bg wow fadeInRight">
                            <div class="title gradient-text">
                                <h6><?=$catalog->name?></h6>
                            </div>
                            <div class="row-list">
                                <div class="back-link">
                                    <a href="/catalog/<?=$catalog->parent->parent->url?$catalog->parent->parent->url.'/':''?><?=$catalog->parent->url?>"><span>&#8592</span>Назад к <?=$catalog->parent->name?></a>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-version-hide filter wow fadeInUp">
                            <div class="row">
                                <div class="col-sm-3 border-right"></div>
                                <div class="col-sm-6  border-right">
                                    <div class="filter-form">
                                        <label for="from">Цена от:</label>
                                        <input type="text" id="from" class="from_price">
                                        <label for="to">Цена до:</label>
                                        <input type="text" id="to" class="to_price">
                                        <label for="to">тг</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox-flex">
                                        <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked" class="v_nalichii">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox" class="akcii">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-version-hide row wow fadeInUp items">
                            <div class="row" id="loadMoreResults">
                                <input type="hidden" value="<?=$catalog->id ?>" class="category_id">
                                <? $m = 0;?>
                                <?foreach($catalog->productsactive as $v){?>
                                    <? $m++;?>
                                    <? if($m > $per_page) break;?>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="catalog-item">
                                            <div class="image">
                                                <?$img = unserialize($v->images);$img = $img[0]?>
                                                <a href="/product/<?=$v->url?>">
                                                    <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                                </a>
                                            </div>
                                            <div class="text-block">
                                                <?if($v->status_products){?>
                                                    <div class="red-label">
                                                        <?=$v->status_products?>
                                                    </div>
                                                <?}?>
                                                <div class="name">
                                                    <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                        <p><?=$v->name?></p>
                                                    </a>
                                                </div>
                                                <div class="status">
                                                    <? if($v->status):?>
                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                    <? endif;?>
                                                    <? if(!$v->status):?>
                                                        <p>Нет в наличии</p>
                                                    <? endif;?>
                                                    <? if(Yii::$app->user->isGuest):?><a href="#"><i class="fas fa-heart favorites" onclick="locateToSignIn()"></i></a>
                                                    <?else:?><i class="fas fa-heart like-active favorites <?=$v->getFavoriteStatus() ? 'favorites-active':'';?>"  data-id="<?=$v->id;?>" ></i>
                                                    <? endif;?>
                                                </div>
                                                <div class="flex-one">
                                                    <div class="price">
                                                        <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                                                    </div>
                                                    <div class="catalog-basket-button">
                                                        <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsForCatalog"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                            </div>
                            <div class="row" id="loadMoreButton">
                                <? if (count($catalog->productsactive) > $per_page) : ?>
                                    <div class="col-sm-12">
                                        <div class="collapse-wrap">
                                            <div class="load-more">
                                                <button>Загрузить еще</button>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalog"></div>
                                <?= $this->render("_bonus_web"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="tablet-version col-sm-12">
                        <div class="main-title gradient-text">
                            <h5>Товар месяца</h5>
                        </div>
                        <div class="row">
                            <?foreach($top_products as $v){?>
                                <div class="col-sm-6">
                                    <div class="month-sale-item">
                                        <a href="">
                                            <div class="top">
                                                <div class="image">
                                                    <?$img = unserialize($v->images);$img = $img[0]?>
                                                    <a href="/product/<?=$v->url?>">
                                                        <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <div class="name">
                                                        <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                            <p><?=$v->name?></p>
                                                        </a>
                                                    </div>
                                                    <div class="price">
                                                        <?if($v->discount){echo '<p>'.$v->price.'</p>';}?>
                                                        <span class="gradient-text"> <?if($v->discount){ $discount = $v->price*(100 - $v->discount)/100; echo $discount;} else echo $v->price;?></span>тг
                                                    </div>
                                                    <div class="status">
                                                        <? if($v->status):?>
                                                            <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                        <? endif;?>
                                                        <? if(!$v->status):?>
                                                            <p>Нет в наличии</p>
                                                        <? endif;?>
                                                        <? if(Yii::$app->user->isGuest):?><a href="#"><i class="fas fa-heart favorites" onclick="locateToSignIn()"></i></a>
                                                        <?else:?><i class="fas fa-heart like-active favorites <?=$v->getFavoriteStatus() ? 'favorites-active':'';?>"  data-id="<?=$v->id;?>" ></i>
                                                        <? endif;?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bot">
                                                <div class="sale">
                                                    <p><?=$v->discount?>%</p>
                                                    <div class="bonus">
                                                        <?=$v->bonus?> Бонус
                                                    </div>
                                                </div>
                                                <div class="sale-basket-button">
                                                    <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div
                            <?}?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div>
                            <div class="main-title gradient-text">
                                <h5>Товар месяца</h5>
                            </div>
                            <?foreach($top_products as $v){?>
                                <div class="month-sale-item">
                                    <a href="">
                                        <div class="top">
                                            <div class="image">
                                                <?$img = unserialize($v->images);$img = $img[0]?>
                                                <a href="/product/<?=$v->url?>">
                                                    <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                                </a>
                                            </div>
                                            <div class="text-block">
                                                <div class="name">
                                                    <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                        <p><?=$v->name?></p>
                                                    </a>
                                                </div>
                                                <div class="price">
                                                    <?if($v->discount){echo '<p>'.$v->price.'</p>';}?>
                                                    <span class="gradient-text"> <?=$v->calculatePrice?></span>тг
                                                </div>
                                                <div class="status">
                                                    <? if($v->status):?>
                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                    <? endif;?>
                                                    <? if(!$v->status):?>
                                                        <p>Нет в наличии</p>
                                                    <? endif;?>
                                                    <? if(Yii::$app->user->isGuest):?><a href="#"><i class="fas fa-heart favorites" onclick="locateToSignIn()"></i></a>
                                                    <?else:?><i class="fas fa-heart like-active favorites <?=$v->getFavoriteStatus() ? 'favorites-active':'';?>"  data-id="<?=$v->id;?>" ></i>
                                                    <? endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bot">
                                            <div class="sale">
                                                <?if($v->discount):?>
                                                    <p><?=$v->discount?>%</p>
                                                <? endif;?>
                                                <?if($v->bonus):?>
                                                    <div class="bonus">
                                                        <?=$v->bonus?> Бонус
                                                    </div>
                                                <? endif;?>
                                            </div>
                                            <div class="sale-basket-button">
                                                <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?}?>
                        </div>
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="owl1 owl-carousel owl-theme">
                            <? foreach ($catalogLevel as $value): ?>
                                <div class="item">
                                    <div class="green" >
                                        <a href="#home<?=$value->id?>"><?= $value->name; ?></a>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="tablet-version-hide-mob filter">
                            <div class="row">
                                <div class="col-sm-3 border-top">
                                </div>
                                <div class="col-sm-3 border-top">
                                    <div class="filter-form">
                                        <label for="fromMob">Цена от:</label>
                                        <input type="text" id="fromMob" class="from_price_mob">
                                        <label for="toMob">Цена до:</label>
                                        <input type="text" id="toMob" class="to_price_mob">
                                        <label for="toMob">тг</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 border-top">
                                    <div class="checkbox-flex">
                                        <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked" class="v_nalichiiMob">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox" class="akciiMob">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-version-hide-mob items">
                            <div class="tab-content">
                                <? foreach ($catalogLevel as $value) : ?>
                                    <? $products = $value->allProducts; ?>
                                    <div class="tab-item <?= $value->id == $id ? "tab-item-active" : ""; ?>" id="home<?= $value->id ?>" data-id="<?= $value->id ?>">
                                        <div class="row" id="loadMoreResultsMob">
                                            <? $m = 0; ?>
                                            <? foreach ($products as $v) { ?>
                                                <? $m++; ?>
                                                <? if ($m > $per_page) break; ?>


                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="catalog-item">
                                                        <div class="image">
                                                            <? $img = unserialize($v->images);$img = $img[0] ?>
                                                            <a href="/product/<?= $v->url ?>">
                                                                <img src="/backend/web/<?= $v->path . $img ?>" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="text-block">
                                                            <? if ($v->status_products) { ?>
                                                                <div class="red-label">
                                                                    <?= $v->status_products ?>
                                                                </div>
                                                            <? } ?>
                                                            <div class="name">
                                                                <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                                    <p><?=$v->name?></p>
                                                                </a>
                                                            </div>
                                                            <div class="status">
                                                                <? if ($v->status) : ?>
                                                                    <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                                <? endif; ?>
                                                                <? if (!$v->status) : ?>
                                                                    <p>Нет в наличии</p>
                                                                <? endif; ?>
                                                                <? if(Yii::$app->user->isGuest):?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                                <?else:?><i class="fas fa-heart like-active favorites <?=$v->getFavoriteStatus() ? 'favorites-active':'';?>"  data-id="<?=$v->id;?>" ></i>
                                                                <? endif;?>
                                                            </div>
                                                            <div class="flex-one">
                                                                <div class="price">
                                                                    <p class="gradient-text"><?= $v->calculatePrice; ?><span>тг.</span></p>
                                                                </div>
                                                                <div class="catalog-basket-button">
                                                                    <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalogMob"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>

                                        <div class="row" id="loadMoreButtonMob">
                                            <? if (count($products) > $per_page) : ?>
                                                <div class="col-sm-12">
                                                    <div class="collapse-wrap">
                                                        <div class="load-more" id="load-more-mob" >
                                                            <button data-quantity-id="<?=$m-1;?>">Загрузить еще</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <div class="row" >
                                            <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalogMob"></div>
                                            <?= $this->render("_bonus_mobile"); ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
