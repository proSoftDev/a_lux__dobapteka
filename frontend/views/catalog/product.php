<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow slideInLeft">
                        <div class="main-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-5 wow fadeInLeft">
                        <div class="product-img">
                            <? $img = unserialize($Product->images);
                            $img = $img[0] ?>
                            <img src="/backend/web/<?= $Product->path . $img ?>" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-sm-7 wow fadeInRight">
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Наличие</p><span class="gradient-text"><?= $Product->status ? 'Есть' : 'Нет' ?> в наличии</span>
                                </li>
                                <li>
                                    <p>Страна</p><span><?= $Product->this_country->name ?></span>
                                </li>
                                <li>
                                    <p>Описание</p><span><?= $Product->model ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button class="btn-add-favorite" data-id="<?= $Product->id; ?>" data-user-id="<?= Yii::$app->user->id ?>">В избранное</button>
                            </div>

                            <div class="basket-button">
                                <button class="btn-in-basket" data-id="<?= $Product->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-12 wow fadeInUp">
                        <div class="instruction">
                            <p data-toggle="collapse" data-target="#demo"><img src="/images/collapse-arrow.png" alt="">Инструкция по применению</p>
                        </div>
                        <div id="demo" class="collapse">
                            <div class="collapse-text">
                                <?= $Product->text ?>
                            </div>
                        </div>

                    </div>

                    <? if ($sop_tovary != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="main-title gradient-text border-top">
                                <h5>Сопутствующие товары</h5>
                            </div>
                        </div>
                        <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                    <? endif; ?>
                    <? if ($analogy != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top">
                                <h5>Аналоги лекарственных средств</h5>
                            </div>
                        </div>
                        <?= $this->render('../partials/analogy', compact('analogy')) ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="product-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                        <div class="product-img">
                            <? $img = unserialize($Product->images);
                            $img = $img[0] ?>
                            <img src="/backend/web/<?= $Product->path . $img ?>" class="img-fluid" alt="">
                        </div>
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Наличие</p><span class="gradient-text"><?= $Product->status ? 'Есть' : 'Нет' ?> в наличии</span>
                                </li>
                                <li>
                                    <p>Страна</p><span><?= $Product->this_country->name ?></span>
                                </li>
                                <li>
                                    <p>Модель</p><span><?= $Product->model ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-9 col-sm-12">
                            <div class="instruction">
                                <p data-toggle="collapse" data-target="#demo"><img src="/images/collapse-arrow.png" alt="">Инструкция по применению</p>
                            </div>
                            <div id="demo" class="collapse instruction-collapse">
                                <div class="collapse-text">
                                    <?= $Product->text ?>
                                </div>
                            </div>


                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button class="btn-add-favorite" data-id="<?= $Product->id; ?>" data-user-id="<?= Yii::$app->user->id ?>">В избранное</button>
                            </div>

                            <div class="basket-button">
                                <button class="btn-in-basket" data-id="<?= $Product->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                    <? if ($sop_tovary != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top">
                                <h5>Сопутствующие товары</h5>
                            </div>
                            <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                        </div>
                    <? endif; ?>

                    <? if ($analogy != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top">
                                <h5>Аналоги лекарственных средств</h5>
                            </div>
                            <?= $this->render('../partials/analogy', compact('analogy')) ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>

</div>
</div>