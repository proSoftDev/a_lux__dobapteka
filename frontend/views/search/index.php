<? use yii\helpers\Url;



?>

<div class="main">
    <div class="desktop-version serch-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-6 col-sm-12" >
                    <div class="row-list" style="margin-bottom: 30px;margin-top: 30px;">
                        <div class="title gradient-text">
                            <?php
                            if($count){?>
                                <div  style="font-size: 16px;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$search?></span>.</div>
                            <?}else{?>
                                <div class="" style="font-size: 22px;">По запросу <span style="font-weight: bold;"><?=$search?></span> ничего не найдено.</div>
                            <?}?>
                        </div>
                    </div>
                    <div class="tablet-version-hide row wow fadeInUp">
                        <? if($product != null):?>
                            <? foreach ($product as $v):?>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <?$img = unserialize($v->images);$img = $img[0]?>
                                            <a href="/product/<?=$v->url?>">
                                                <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <?if($v->status_products){?>
                                                <div class="red-label">
                                                    <?=$v->status_products?>
                                                </div>
                                            <?}?>
                                            <div class="name">
                                                <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                    <p ><?=$v->name?></p>
                                                </a>
                                            </div>
                                            <div class="status">
                                                <p><?if($v->status==1){?><img src="/images/ok.png" alt=""> Есть<?}else{?>Нет<?}?>
                                                    в наличии</p>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                                                </div>
                                                <div class="catalog-basket-button">
                                                    <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach;?>
                        <? endif;?>
                    </div>
                </div>

                <div class="tablet-version col-sm-12">
                    <div class="row">
                        <? if($product != null):?>
                            <? foreach ($product as $v):?>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <?$img = unserialize($v->images);$img = $img[0]?>
                                            <a href="/product/<?=$v->url?>">
                                                <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <?if($v->status_products){?>
                                                <div class="red-label">
                                                    <?=$v->status_products?>
                                                </div>
                                            <?}?>
                                            <div class="name">
                                                <p><?=$v->name?></p>
                                            </div>
                                            <div class="status">
                                                <p><?if($v->status==1){?><img src="/images/ok.png" alt=""> Есть<?}else{?>Нет<?}?>
                                                    в наличии</p>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                                                </div>
                                                <div class="catalog-basket-button">
                                                    <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach;?>
                        <? endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="mobile-version mobile-version-search">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="row-list" style="margin-bottom: 30px">
                            <div class="title gradient-text">
                                <?php
                                if($count){?>
                                    <div  style="font-size: 16px;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$search?></span>.</div>
                                <?}else{?>
                                    <div class="" style="font-size: 22px;">По запросу <span style="font-weight: bold;"><?=$search?></span> ничего не найдено.</div>
                                <?}?>
                            </div>
                        </div>
                        <div class="tab-content tablet-version-hide-mob items">
                            <div class="tab-item tab-item-active" id="home">
                                <div class="row">
                                    <? if($product != null):?>
                                    <?foreach($product as $v){?>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="catalog-item">
                                                <div class="image">
                                                    <?$img = unserialize($v->images);$img = $img[0]?>
                                                    <a href="/product/<?=$v->url?>">
                                                        <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <?if($v->status_products){?>
                                                        <div class="red-label">
                                                            <?=$v->status_products?>
                                                        </div>
                                                    <?}?>
                                                    <div class="name">
                                                        <p><?=$v->name?></p>
                                                    </div>
                                                    <div class="status">
                                                        <p><?if($v->status==1){?><img src="/images/ok.png" alt=""> Есть<?}else{?>Нет<?}?>
                                                            в наличии</p>
                                                    </div>
                                                    <div class="flex-one">
                                                        <div class="price">
                                                            <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                                                        </div>
                                                        <div class="catalog-basket-button">
                                                            <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?}?>
                                    <? endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


