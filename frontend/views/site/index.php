<div class="main">
    <div class="desktop-version">
        <div class="slider">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 wow fadeInLeft">
                        <div class="catalog-menu">
                            <div class="title gradient-text">
                                Каталог товаров
                            </div>
                            <?=$result?>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 wow fadeInRight">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <? $class = 'active';?>
                                <? $m = 0;?>
                                <? foreach ($banner as $v):?>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="<?=$m;?>" class="<?=$class?>"></li>
                                    <? $class = '';?>
                                    <? $m++;?>
                                <? endforeach;?>
                            </ol>
                            <div class="carousel-inner">
                                <? $class = 'active';?>
                                <? foreach ($banner as $v):?>
                                    <div class="carousel-item <?=$class;?>">
                                        <img class="d-block w-100" src="<?=$v->getImage();?>" alt="First slide">
                                        <div class="carousel-caption">
                                            <?=$v->text;?>
                                        </div>
                                        <div class="green-caption">
                                            <img src="/images/green-caption.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <? $class = '';?>
                                <? endforeach;?>
                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <img src="images/left.png" alt="">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <img src="images/right.png" alt="">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-title gradient-text wow fadeInUp">
                            <p><?=$titles[1]->name;?></p>
                        </div>
                    </div>
                    <? foreach ($advantages as $v):?>
                        <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                            <div class="adv-item">
                                <a href="advantage/<?=$v->url;?>">
                                    <div class="adv-icon">
                                        <img src="<?=$v->getImage();?>" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6><?=$v->name;?></h6>
                                        <p><?=$v->text;?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <? endforeach;?>



					<? if($hitProduct != null):?>
                    <div class="col-sm-12">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text wow fadeInUp">
                            <h5><?=$titles[2]->name;?></h5>
                        </div>
                    </div>
                    <? foreach ($hitProduct as $v):?>
                        <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                            <div class="top-sale-item">
                                <a href="product-card.php">
                                    <div class="hit">
                                        <img src="images/hit.png" alt="">
                                    </div>
                                    <?$img = unserialize($v->images);$img = $img[0]?>
                                    <a href="/product/<?=$v->url?>">
                                        <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                    </a>
                                    <div class="name">
                                        <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                            <p><?=$v->name?></p>
                                        </a>
                                    </div>
                                    <div class="price">
                                        <p class="gradient-text"><?=$v->calculatePrice;?><span>тг.</span></p>
                                    </div>
                                    <div class="basket-button">
                                        <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsForHit"><img src="/images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <? endforeach;?>
                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForHit"></div>
					<? endif;?>


					<? if($newProduct != null):?>
                    <div class="col-sm-12">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text wow fadeInUp">
                            <h5><?=$titles[3]->name;?></h5>
                        </div>
                    </div>
                    <? foreach ($newProduct as $v):?>
                        <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                            <div class="link-wrap">
                                <a href="product-card.php">
                                    <div class="new-item">
                                        <div class="image">
                                            <?$img = unserialize($v->images);$img = $img[0]?>
                                            <a href="/product/<?=$v->url?>">
                                                <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <div class="name">
                                                <a href="/product/<?=$v->url?>" style="text-decoration: none;">
                                                    <p ><?=$v->name?></p>
                                                </a>
                                            </div>
                                            <div class="status">
                                                <? if($v->status):?>
                                                    <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                <? endif;?>
                                                <? if(!$v->status):?>
                                                    <p>Нет в наличии</p>
                                                <? endif;?>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?=$v->calculatePrice;?><span>тг.</span></p>
                                                </div>
                                                <div class="news-basket-button">
                                                    <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsForNew"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <? endforeach;?>
                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForNew"></div>
					<? endif;?>

                    <div class="col-sm-12">
<!--                        <div class="all-catalog wow fadeInUp">-->
<!--                            <button>Весь каталог</button>-->
<!--                        </div>-->
                        <div class="title-border"></div>
                        <div class="main-title gradient-text">
                            <h5>Как нас найти</h5>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="map-block">
                            <div class="map wow fadeInLeft">
                                <div id="map" style="width: 100%; height: 480px;border:0" ></div>
                                
                                <script>
                                  
                                    ymaps.ready(init);
                                    var center_map = [0, 0];
                                    var map = "";
                                    function init() {
                                        map = new ymaps.Map('map', {
                                            center: center_map,
                                            zoom: 10,
                                        });
                                        map.behaviors.disable('scrollZoom');

                                        var myGeocoder = ymaps.geocode("<?=Yii::$app->session["city_name"];?>");
                                        myGeocoder.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var coords = street.geometry.getCoordinates();
                                                map.setCenter(coords);
                                            },
                                            function (err) {

                                            }
                                        );

                                        <?foreach($filial as $v){?>
                                        map.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                                            balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                                        }, {
                                            iconLayout: 'default#image',
                                            iconImageHref: '/images/loc-icon.png',
                                            preset: 'islands#icon',
                                            iconColor: '#0095b6'
                                        }));
                                        <?}?>
                                    }
                                </script>
                            </div>
                            <div class="drugstore-list wow fadeInRight">
                                <?foreach($filial as $v):?>
                                    <div class="drugstore-item">
                                        <p><img src="/images/loc-icon.png" alt=""><b><?=$v->address;?></b></p>
                                        <? try {
                                            $from_time = new DateTime($v->from_time);
                                            $to_time = new DateTime($v->to_time);
                                        } catch (Exception $e) {
                                        } ?>
                                        <p><img src="/images/clock.png" alt=""><?=$from_time->format('H:i');?> - <?=$to_time->format('H:i');?> ч.</p>
                                        <p><img src="/images/phone-icon.png" alt=""><?=$v->telephone;?> </p>
                                        <? if($v->currentStatus == 'Открыто'):?>
                                            <div class="status-open">
                                                <?=$v->currentStatus;?>
                                            </div>
                                        <? endif?>
                                        <? if($v->currentStatus == 'Закрыто'):?>
                                            <div class="status-close">
                                                <?=$v->currentStatus;?>
                                            </div>
                                        <? endif?>
                                    </div>
                                <? endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="slider">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-sm-12">
                        <div class="owl1 owl-carousel owl-theme">
                            <? foreach ($catalog as $value): ?>
                            <div class="item">
                                <div class="green" >
                                    <a href="/catalog/<?= $value->url?>"><?= $value->name; ?></a>
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>

                    </div>
                    <div class="col-sm-12 pl-0 pr-0">
                        <div id="carouselExampleIndicators2" class="desktop-version carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <? $class = 'active';?>
                                <? $m = 0;?>
                                <? foreach ($banner as $v):?>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="<?=$m;?>" class="<?=$class?>"></li>
                                    <? $class = '';?>
                                    <? $m++;?>
                                <? endforeach;?>
                            </ol>

                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                <img src="images/left.png" alt="">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                <img src="images/right.png" alt="">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="owl-main owl-carousel owl-theme mobile-version owl-main-mobile">
                            <? $class = 'active';?>
                            <? foreach ($banner as $v):?>
                            <div class="item">
                                <img class="d-block w-100 img-fluid" src="<?=$v->getImage();?>" alt="First slide">
                                <div class="carousel-caption">
                                    <?=$v->text;?>
                                </div>
                                <div class="green-caption">
                                    <img src="images/green-caption.png" class="img-fluid" alt="">
                                </div>
                            </div>
                                <? $class = '';?>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-sm-12">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text">
                            <h5><?=$titles[1]->name;?></h5>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="square-flex">
                            <? foreach ($advantages as $v):?>
                            <div class="adv-item">
                                <a href="">
                                    <div class="adv-icon">
                                        <img src="<?=$v->getImage();?>" alt="">
                                    </div>
                                    <div class="text-block">
                                        <h6><?=$v->name;?></h6>
                                        <p><?=$v->text;?></p>
                                    </div>
                                </a>
                            </div>
                            <? endforeach;?>
                        </div>
                    </div>

                    <? if($hitProduct != null):?>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text">
                            <h5><?=$titles[2]->name;?></h5>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp ">
                        <div class="square-flex">
                            <? foreach ($hitProduct as $v):?>
                                <div class="top-sale-item">
                                    <a href="product-card.php">
                                        <div class="hit">
                                            <img src="images/hit.png" alt="">
                                        </div>
                                        <?$img = unserialize($v->images);$img = $img[0]?>
                                        <a href="/product/<?=$v->url?>">
                                            <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                        </a>
                                        <div class="name">
                                            <p><?=$v->name?></p>
                                        </div>
                                        <div class="price">
                                            <p class="gradient-text"><?=$v->calculatePrice;?><span>тг.</span></p>
                                        </div>
                                        <div class="basket-button">
                                            <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsForHitMob"><img src="/images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </a>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForHitMob"></div>
                    <? endif;?>



                    <? if($newProduct != null):?>
                    <div class="col-sm-12 wow fadeInUp ">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text">
                            <h5><?=$titles[3]->name;?></h5>
                        </div>
                    </div>
                    <? foreach ($newProduct as $v):?>
                    <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp">
                        <div class="new-item">
                            <div class="image">
                                <?$img = unserialize($v->images);$img = $img[0]?>
                                <a href="/product/<?=$v->url?>">
                                    <img src="/backend/web/<?=$v->path.$img?>" alt="">
                                </a>
                            </div>
                            <div class="text-block">
                                <div class="name">
                                    <p><?=$v->name?></p>
                                </div>
                                <div class="status">
                                    <? if($v->status):?>
                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                    <? endif;?>
                                    <? if(!$v->status):?>
                                        <p>Нет в наличии</p>
                                    <? endif;?>
                                </div>
                                <div class="flex-one">
                                    <div class="price">
                                        <p class="gradient-text"><?=$v->calculatePrice;?><span>тг.</span></p>
                                    </div>
                                    <div class="news-basket-button">
                                        <button class="btn-in-basket" data-id="<?=$v->id;?>" data-slider-id="relatedProductsForNewMob"><img src="/images/light-basket.png" alt="">В корзину</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? endforeach;?>
                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForNewMob"></div>
                    <? endif;?>



                    <div class="col-sm-12 wow fadeInUp">
                        <div class="title-border"></div>
                        <div class="main-title gradient-text">
                            <h5>Как нас найти</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map wow fadeInUp">
            <div id="mapMob" style="width: 100%; height: 480px;border:0" ></div>
            <script>

            ymaps.ready(init);
            var center_map = [0, 0];
            var mapMob = "";
            function init() {
                mapMob = new ymaps.Map('mapMob', {
                    center: center_map,
                    zoom: 10,
                });

                var myGeocoder = ymaps.geocode("<?=Yii::$app->session['city_name'];?>");
                myGeocoder.then(
                    function (res) {
                        var street = res.geoObjects.get(0);
                        var coords = street.geometry.getCoordinates();
                        mapMob.setCenter(coords);
                    },
                    function (err) {

                    }
                );

                <?foreach($filial as $v){?>
                mapMob.geoObjects.add(new ymaps.Placemark([<?=$v->latitude?>, <?=$v->longitude?>], {
                    balloonContent: '<div class="company-name wow fadeInUp"><?=$v->address?></br><?=$v->telephone?></div>'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/images/loc-icon.png',
                    preset: 'islands#icon',
                    iconColor: '#0095b6'
                }));
                <?}?>
            }
            </script>
        </div>
    </div>

</div>
</div>
