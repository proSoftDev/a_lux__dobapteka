<div class="owl1 owl-carousel owl-theme ">
    <?foreach ($sop_tovary as $v){?>
        <div class="item">
            <div class="top-sale-item">
                <?$img = unserialize($v->images);$img = $img[0]?>
                <a href="/product/<?=$v->url?>">
                    <img src="/backend/web/<?=$v->path.$img?>" alt="">
                </a>
                <div class="name">
                    <p><?=$v->name?></p>
                </div>
                <div class="price">
                    <p class="gradient-text"><?=$v->price?> <span>тг.</span></p>
                </div>
                <div class="basket-button">
                    <button class="btn-in-basket" data-id="<?=$v->id;?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                </div>
            </div>
        </div>
    <?}?>
</div>
