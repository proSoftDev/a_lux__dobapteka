<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.12.2019
 * Time: 16:58
 */

namespace app\models;


use yii\base\Model;

class OrderGuestForm extends Model
{
    public $fio;
    public $email;
    public $telephone;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone'], 'required'],
            ['email', 'email'],
            [['fio',],'string','max' => 255]

        ];
    }

    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'email' => 'E-mail',
            'telephone' => 'Телефон',
        ];
    }




}
