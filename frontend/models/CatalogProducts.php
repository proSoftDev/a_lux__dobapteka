<?php
namespace app\models;

use Yii;

class CatalogProducts extends \yii\db\ActiveRecord
{
    public $path = 'images/catalogproducts/';

    public static function tableName()
    {
        return 'catalog_products';
    }

    public function rules()
    {
        return [
            [['parent_id', 'sort', 'status', 'created_at'], 'integer'],
            [['title', 'description', 'name', 'url', 'sort', 'status', 'created_at'], 'required'],
            [['description'], 'string'],
            [['title', 'name', 'url', 'img'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title' => 'Заголовок',
            'description' => 'Description',
            'name' => 'Название',
            'url' => 'Url',
            'img' => 'Картинка',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'created_at' => 'Created At',
        ];
    }

    public function upload()
    {
        $time = time();
        $this->img->saveAs($this->path. $time . $this->img->baseName . '.' . $this->img->extension);
        return $time . $this->img->baseName . '.' . $this->img->extension;
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = CatalogProducts::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }
}
