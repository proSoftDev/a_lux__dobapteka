<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       "/css/bootstrap.css",
       "/css/owl.carousel.min.css",
       "/css/owl.theme.default.min.css",
       "/css/animate.css",
       "/css/style.css",
       "/css/smoothbox.css",
       "/css/hc-offcanvas-nav.css",
        "/css/font-awesome-animation.css",
        "/css/awesomplete.css",
       "https://use.fontawesome.com/releases/v5.7.1/css/all.css" ,
       "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css",
        "https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.css"
    ];
    public $js = [
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js",
        "/js/bootstrap.min.js",
        "/js/owl.carousel.min.js",
        "/js/hc-offcanvas-nav.js",
        "/js/account.js",
        "/js/scripts.js",
        "/js/search.js",    
        "/js/main.js",
        "/js/basket.js",
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
