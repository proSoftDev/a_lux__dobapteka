<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:32
 */

namespace frontend\controllers;


use app\models\OrderGuestForm;
use common\models\DiscountEarliestPersons;
use common\models\Filial;
use common\models\Geocoords;
use common\models\Orders;
use common\models\PriceFreeDelivery;
use common\models\Products;
use common\models\PromoCode;
use common\models\PromoUsers;
use common\models\Text;
use common\models\UserAddedProduct;
use common\models\UserBalance;
use common\models\UserGift;

use Yii;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

class CardController extends FrontendController
{
    public function actionIndex(){

        $_SESSION['deliveryPrice'] = 0;
        $_SESSION['promo'] = 0;
        $_SESSION['earliest'] = 0;

        if(!Yii::$app->user->isGuest) {
            $bonus = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);
            $_SESSION['earliest'] = DiscountEarliestPersons::getContent()->discountPercent;
        }else{
            $order = new Orders();
            Yii::$app->view->params['order'] = $order;
        }


        unset($_SESSION['promo_id']);
        unset($_SESSION['bonus']);
        unset($_SESSION['selectedAddress']);
        unset($_SESSION['guest_data']);

        $this->setMeta('Корзина');
        $sum = Products::getSum();
        $model = Geocoords::getAll();
        $address = Filial::findAll(['city_id'=>Yii::$app->session->get('city_id')]);
        $text = Text::findAll(['type_id' => 2]);

        return $this->render('basket',compact('sum','model','address','bonus','text'));
    }


    public function actionGift()
    {
        if (count($_SESSION['gift_for_type']) != null){
            if ($_SESSION['gift_for_type'] == 1) {
                return $this->render('gift_for_price');
            } elseif ($_SESSION['gift_for_type'] == 0) {
                return $this->render('gift_for_product');
            } elseif ($_SESSION['gift_for_type'] == 2) {
                return $this->render('gift_for_price');
            } else {
                return $this->goHome();
            }
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionDeleteProduct($id)
    {
        if(!Yii::$app->user->isGuest) UserAddedProduct::deleteAll(['user_id'=>Yii::$app->user->id, 'product_id'=>$id]);
        $m=0;
        foreach ($_SESSION['basket'] as $v){
            if($v->id == $id){
                unset($_SESSION['basket'][$m]);
                $_SESSION['basket'] = array_values($_SESSION['basket']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }


    public function actionDeleteGift($id)
    {
        $m=0;
        foreach ($_SESSION['gift'] as $v){
            if($v->id == $id){
                unset($_SESSION['gift'][$m]);
                $_SESSION['gift'] = array_values($_SESSION['gift']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }

    public function actionAddProduct()
    {
        if (Yii::$app->request->isAjax) {
            $id = $_GET['id'];
            if (!Yii::$app->user->isGuest) UserAddedProduct::addProduct(Yii::$app->user->id, $id);
            $basket = Products::findOne($id);
            $basket->count = 1;
            $check = true;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $id) {
                    $check = false;
                    $_SESSION['basket'][$k]['count'] += 1;
                    break;
                }
            }
            if ($check) array_push($_SESSION['basket'], $basket);
            $count = count($_SESSION['basket']) + count($_SESSION['gift']);
            $array = ['status' => 1, 'count' => $count];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionAddGift()
    {
        if (Yii::$app->request->isAjax) {
            $basket = UserGift::findAll(['user_id' => Yii::$app->user->id]);
            foreach ($basket as $val) {
                $check = true;
                $product = $val->product;
                foreach ($_SESSION['gift'] as $v) {
                    if ($product->id == $v->id) {
                        $check = false;
                        break;
                    }
                }
                if ($check) {
                    $product->count = $val->quantity;
                    $product->price = 0;
                    array_push($_SESSION['gift'], $product);
                }
            }

            $count = count($_SESSION['basket']) + count($_SESSION['gift']);
            $array = ['status' => 1, 'count' => $count];

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionDownClicked()
    {
        if (Yii::$app->request->isAjax) {
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id'] && $_SESSION['basket'][$k]->count > 1) {
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);
            $array = ['countProduct' => $count, 'sum' => intval($sum), 'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionUpClicked()
    {
        if (Yii::$app->request->isAjax) {
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    $_SESSION['basket'][$k]['count'] += 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);
            $array = ['countProduct' => $count, 'sum' => intval($sum), 'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionCountChanged()
    {
        if (Yii::$app->request->isAjax) {
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    if ($_GET['v'] > 0) {
                        $_SESSION['basket'][$k]['count'] = $_GET['v'];
                    } else {
                        $_SESSION['basket'][$k]['count'] = 1;
                    }
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);
            $array = ['sum' => intval($sum), 'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionOrder()
    {
        if(($_SESSION['basket'] != null ||  $_SESSION['gift'] != null) && Yii::$app->request->isAjax ) {

            $order = new Orders();
            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);

            if (!Yii::$app->user->isGuest) {
                $used_promo = Orders::getUsedPromo();
                $used_bonus = Orders::getUsedBonus();
            } else {
                $used_promo = 0;
                $used_bonus = 0;
            }

            if (Yii::$app->user->isGuest && $_SESSION['guest_data'] == null) return Orders::clientIsGuest;
            if ($_GET['deliveryMethod'] == 1) {
                if (Yii::$app->user->isGuest && $_SESSION['guest_data'] == null) return Orders::clientIsGuest;
                else {
                    $deliveryPrice = $this->getDeliveryPrice();
                    $json = Orders::IntegrationWithRelogSystem(intval($sum), $used_bonus, $used_promo, $deliveryPrice);
                    if ($json) {
                        $data = json_decode($json);
                        if ($data->status == 'success') {
                            $delivery_id = $data->_id;
                            return $order->saveOrderWithDelivery($delivery_id, intval($sum), $used_bonus, $used_promo, $_GET['change'], $_GET['address'], $deliveryPrice, $_GET['paymentMethod']);
                        } else {
                            return $data->error;
                        }
                    }
                }
            } elseif ($_GET['deliveryMethod'] == 2) {
                if ($_SESSION['selectedAddress'] == null) return 'Выберите пункт который вы будете забрать заказ.';
                else return $order->saveOrder(intval($sum), $used_bonus, $used_promo, $_GET['change'], $_GET['paymentMethod']);
            } else {
                return 'Неправильно выбран способ доставки!';
            }

        }else{
            throw new NotFoundHttpException();
        }

    }






    public function actionConfirmOrderedGuestData(){

        if (Yii::$app->request->isAjax) {
            $model = new OrderGuestForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $phoneStatus = true;
                for ($i = 0; $i < strlen($model->telephone); $i++) {
                    if ($model->telephone[$i] == '_') {
                        $phoneStatus = false;
                    }
                }
                if ($phoneStatus) {
                    $_SESSION['guest_data'] = $model;
                    return 1;
                } else {
                    return "Необходимо заполнить «Телефон».";
                }

            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionDelivery()
    {
        $this->setMeta('Корзина');
        return $this->render('delivery', compact('model'));
    }


    public function actionCheckPromo(){
        if (Yii::$app->request->isAjax) {
            date_default_timezone_set('Asia/Almaty');
            $check = PromoCode::find()->where('quantity>0 AND code="' . $_GET['code'] . '"')->one();
            if ($check) {
                $checkUsedPromo = PromoUsers::find()->where('user_id=' . Yii::$app->user->id . ' AND promo_id=' . $check->id)->one();
                if ($checkUsedPromo) {
                    $response['status'] = 0;
                    $response['error'] = "Промокод уже использован.";
                    return json_encode($response);
                } else {
                    $start_time = strtotime($check->start_date);
                    $end_time = strtotime($check->end_date);
                    $time_now = strtotime(date('Y-m-d H:i:s'));
                    if ($start_time < $time_now && $end_time > $time_now) {
                        $_SESSION['promo'] = $check->percent;
                        $_SESSION['promo_id'] = $check->id;
                        $deliveryPrice = $this->getDeliveryPrice();
                        $sum = $this->getSumBasket($deliveryPrice);
                        $response = ['status' => 1, 'percent' => $check->percent, 'sum' => intval($sum), 'deliveryPrice' => $deliveryPrice];
                        return json_encode($response);
                    } else {
                        $response['status'] = 0;
                        $response['error'] = "Промокод не найдено.";
                        return json_encode($response);
                    }
                }

            } else {
                $response['status'] = 0;
                $response['error'] = "Промокод не найдено.";
                return json_encode($response);
            }
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionUpdateSession(){
        if (Yii::$app->request->isAjax) {
            $_SESSION['deliveryPrice'] = $_GET['deliveryPrice'];
            $_SESSION['deliveryTime'] = $_GET['deliveryTime'];
            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $response = ['status' => 1, 'sum' => intval($sum), 'deliveryPrice' => $deliveryPrice];
            return json_encode($response);
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionBonus(){
        if (Yii::$app->request->isAjax) {
            $deliveryPrice = $this->getDeliveryPrice();
            if ($_GET['status'] == 1) {
                $_SESSION['bonus'] = 1;
                $sum = $this->getSumBasket($deliveryPrice);
            } else {
                unset($_SESSION['bonus']);
                $sum = $this->getSumBasket($deliveryPrice);
            }
            $response = ['status' => 1, 'sum' => intval($sum), 'deliveryPrice' => $deliveryPrice];
            return json_encode($response);
        }else{
            throw new NotFoundHttpException();
        }
    }





    public function actionCheckGift(){
        if (Yii::$app->request->isAjax) {

            $id = $_GET['id'];
            $type = $_GET['type'];
            $_SESSION['gift_id'] = $id;
            if ($type == 1) $gifts = $_SESSION['gift_for_price'];
            if ($type == 0) $gifts = $_SESSION['gift_for_product'];
            return $this->renderAjax('gift_result', compact('id', 'gifts', 'error', 'type'));
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionGetSelectAddress(){
        if (Yii::$app->request->isAjax) {
            $id = $_GET['id'];
            $model = Filial::findOne(['city_id' => Yii::$app->session->get('city_id'), 'id' => $id]);
            return $model->address;
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionSetSelectAddress(){
        if (Yii::$app->request->isAjax) {
            $_SESSION['selectedAddress'] = $_GET['address'];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionSaveGift(){
        if (Yii::$app->request->isAjax) {
            if (!isset($_SESSION['gift_id'])) {
                return 'Выберите товар.';
            } else {
                $product = Products::findOne(['id' => $_SESSION['gift_id']]);
                $check = UserGift::findOne(['product_id' => $product->id, 'user_id' => Yii::$app->user->id]);
                if ($check) {
                    $gift = $check;
                    $gift->quantity = $gift->quantity + 1;
                } else {
                    $gift = new UserGift();
                    $gift->quantity = 1;
                }
                $gift->user_id = Yii::$app->user->id;
                $gift->product_id = $product->id;

                if ($gift->save(false)) {
                    if ($_SESSION['gift_for_type'] == 2) {
                        unset($_SESSION['gift_id']);
                        unset($_SESSION['gift_for_price']);
                        $_SESSION['gift_for_type'] = 0;
                        return 2;
                    } else {
                        unset($_SESSION['gift_id']);
                        unset($_SESSION['gift_for_price']);
                        unset($_SESSION['gift_for_product']);
                        unset($_SESSION['gift_for_type']);
                        return 1;
                    }
                } else {
                    return 'Упс, что-то пошло не так!';
                }

            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function getDeliveryPrice(){
        $sum = $this->getSumBasketWithoutDelivery();
        $check_free_delivery = PriceFreeDelivery::getContent();
        if($check_free_delivery->price < $sum){
            return 0;
        }else{
            return $_SESSION['deliveryPrice'];
        }
    }

    public function getSumBasket($deliveryPrice){
        $sum = (Products::getSum())*((100 - $_SESSION['earliest']) / 100);
        if(isset($_SESSION['promo_id'])) {
            $sum = $sum * ((100 - $_SESSION['promo']) / 100);
        }
        $sum = $sum + $deliveryPrice;
        return $sum;
    }

    public function actionSetPaymentChoice(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->session['payment_choice'] = $_GET['payment_choice'];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionSetDeliveryChoice(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->session['delivery_choice'] = $_GET['delivery_choice'];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public static function getSumBasketWithoutDelivery(){
        $sum = (Products::getSum())*((100 - $_SESSION['earliest']) / 100);
        if(isset($_SESSION['promo_id'])) {
            $sum = $sum * ((100 - $_SESSION['promo']) / 100);
        }
        return $sum;
    }



}
