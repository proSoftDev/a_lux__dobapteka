<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.08.2019
 * Time: 16:19
 */

namespace frontend\controllers;


use common\models\Products;

class SearchController extends FrontendController
{
    public function actionIndex($text)
    {
        $this->setMeta('Поиск');
        $search = $text;
        if($text){
            $check = Products::find()->where(['name' => $text])->one();
            if($check) $product = Products::find()->where("name LIKE '%$text%'")->all();
            elseif($_SESSION['searchProducts'] != null) $product = Products::find()->where('id in ('.implode(',',$_SESSION['searchProducts']).')')->all();
            $count = count($product);
        }else{
            $count = 0;
            return $this->render('index', compact('search', 'count', 'text'));
        }

        return $this->render('index', compact('product','count','search'));
    }



}
