<?php
namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\models\Products;

/**
 * Site controller
 */
class ImportController extends FrontendController
{
	public function actionRun() {
		
		
		if (($handle = fopen("../../standart_n/warebase.csv", "r")) !== FALSE) {
			$count = 0;
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				
				if(!$count) {$count++;continue;}
				
        		$data = array_map(function($str) {
					return iconv( "Windows-1251", "UTF-8", $str );
				}, $data );
				
				if(!($model = Products::find()->where(['title' => $data[2]])->one())) {
                	$model = new Products();
				}

                $model_data = [
                    'category_id' => 3,
                    'title' => !empty($data[2]) ? $data[2] : '',
                    'description' => !empty($data[12]) ? $data[12] : '',
                    'name' => !empty($data[2]) ? $data[2] : '',
                    'status' => !((int)$data[7]<=0),
                    'price' => !empty($data[13]) ? (int)$data[13] : 0,
                    'text' => !empty($data[12]) ? $data[12] : '',
                    'country' => '',
                    'model' => '',
					'isHit' => 0,
					'isNew' => 0
                ];

                $model->attributes = $model_data;
                $model->url = $model->generateCyrillicToLatin();
                $model->save(false);
                $model->url = $model->url.'_'.$model->id;
                $model->save(false);
            }
			fclose($handle);
		}
	}
}