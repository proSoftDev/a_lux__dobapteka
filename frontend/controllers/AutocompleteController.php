<?php


namespace frontend\controllers;

use common\models\Products;
use yii\web\Controller;

class AutocompleteController extends Controller
{
    public function actionIndex()
    {
        $keyword = $this->correctTypo($_GET['name_startsWith']);
        $array_isset = [];
        $array = [];
        $inc = 0;

        $query = "name LIKE '%$keyword%'";

        for($i=2;$i<strlen($keyword);$i+=2){
            $val = substr($keyword,0,$i).'-'.substr($keyword,$i,strlen($keyword));
            $query .= " OR name LIKE '%$val%'";
        }

        $ru = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э', 'ю', 'я'];
        for ($i = 0; $i < strlen($keyword); $i += 2) {
            foreach ($ru as $v){
                $val = substr($keyword,0,$i).''.$v.''.substr($keyword,$i+2,strlen($keyword));
                $query .= "OR name LIKE '%$val%'";
            }

        }


//        for ($i = 0; $i < strlen($keyword) - 2; $i += 2) {
//            $val = substr($keyword, $i, strlen($keyword));
//            $query .= "OR name LIKE '%$val'";
//        }

        $products = Products::find()->where("name LIKE '%$keyword%'")->all();
        foreach ($products as $v) {
            if (!in_array($v->id, $array_isset)) {
                $array_isset[] = $v->id;
                $array[] = ['value' =>  $v->name, 'title' => $v->name];
                $inc++;
            }
            if ($inc >= 5) break;
        }



        if (!($inc >= 5)){
            $products = Products::find()->where($query)->all();
            foreach ($products as $v) {
                if (!in_array($v->id, $array_isset)) {
                    $array_isset[] = $v->id;
                    $array[] = ['id' => 1, 'title' => $v->name];
                    $inc++;
                }
                if ($inc >= 5) break;
            }
        }





        $json = json_encode(array('users' => $array));

        if ($_GET['callback']) {
            $callback = $_GET['callback'];
            $json = $callback . '('. $json . ')';
        }

        $_SESSION['searchProducts'] = $array_isset;
        return $json;
    }

    public function correctTypo($text){
        $lat = [
            'q','w','e','r','t','y','u','i','o','p','[',']', 'Q','W','E','R','T','Y','U','I','O','P','{','}',
            'a','s','d','f','g','h','j','k','l',';','\'', 'A','S','D','F','G','H','J','K','L',':','"',
            'z','x','c','v','b','n','m',',','.', 'Z','X','C','V','B','N','M','<','>',
        ];
        $cyr = [
            'й','ц','у','к','е','н','г','ш','щ','з','х','ъ', 'й','ц','у','к','е','н','г','ш','щ','з','х','ъ',
            'ф','ы','в','а','п','р','о','л','д','ж','э',  'ф','ы','в','а','п','р','о','л','д','ж','э',
            'я','ч','с','м','и','т','ь','б','ю',  'я','ч','с','м','и','т','ь','б','ю',
        ];
        return str_replace($lat, $cyr, $text);
    }


}
