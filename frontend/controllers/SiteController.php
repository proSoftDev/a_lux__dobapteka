<?php
namespace frontend\controllers;



use common\models\Advantages;
use common\models\Banner;
use common\models\CatalogProducts;
use common\models\City;
use common\models\Filial;
use common\models\Mainsub;
use common\models\Menu;
use common\models\Products;
use common\models\Review;
use DateTime;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@frontend/web/fonts/Century Gothic.ttf',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionIndex(){

        $this->setMeta('Главная страница');
        $titles = Mainsub::find()->all();
        $banner = Banner::find()->all();
        $advantages = Advantages::find()->all();
        $filial = Filial::find()->where('city_id='.Yii::$app->session["city_id"])->all();
        $result = $this->Catalog(1, 2);
        $hitProduct = Products::findAll(['isHit' => 1]);
        $newProduct = Products::findAll(['isNew' => 1]);
		$catalog = CatalogProducts::find()->where('status = 1 AND level = 1')->all();

        return $this->render('index',compact('titles','banner','advantages','filial', 'result','newProduct','hitProduct', 'catalog'));
    }

    public function actionIndex2($url){
        $page = Menu::findOne(['url' => $url]);
        $this->setMeta($page->metaName,$page->metaKey, $page->metaDesc);
        if($page) return $this->render('index2',compact('page'));
        else throw new NotFoundHttpException();
    }



	
	
}
