<?php

namespace frontend\controllers;


use common\models\Orderedproduct;
use common\models\Orders;
use common\models\Products;
use Yii;
use Paybox\Pay\Facade as PayboxApi;
use yii\web\NotFoundHttpException;

class PayboxController extends FrontendController
{
    public function actionIndex()
    {
        $order = Orders::findByToken();
        if($order){
            $order->deleteAccessTokenSession();
            $order->save(false);
            $component = Yii::$app->paybox;
            $component->Index($order->id, 'Оплата заказа №'.$order->id, $order->sum,
                Yii::$app->user->isGuest ? preg_replace('/[^a-zA-Z0-9]/','', $order->telephone) :
                    preg_replace('/[^a-zA-Z0-9]/','', $order->user->username),
                Yii::$app->user->isGuest ? $order->email : $order->user->email);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }else{
            throw new NotFoundHttpException();
        }

    }


//    public function Check()
////    {
////        $paybox = new PayboxApi();
////        echo $paybox->cancel('Ошибка');
////        die;
////    }

    public function actionResult()
    {
        if($_GET['pg_can_reject'] == 1){
            $Order = Orders::findOne($_GET['pg_order_id']);
            $Order->saveOrdersByPayStatusTrue();
        }else{
            $paybox = new PayboxApi();
            echo $paybox->cancel('Ошибка');
        }
        die;
    }

    public function actionSuccess()
    {
        $Order = Orders::findOne($_GET['pg_order_id']);
        if($Order->statusPay){
            if($_SESSION['paybox_order_response'] == 1){
                unset($_SESSION['paybox_order_response']);
                return $this->redirect("/account/?tab=orders");
            }else if($_SESSION['paybox_order_response'] == 2){
                unset($_SESSION['paybox_order_response']);
                return $this->redirect('card/gift');
            }else if($_SESSION['paybox_order_response'] == 3){
                unset($_SESSION['paybox_order_response']);
                return $this->redirect(Yii::$app->homeUrl);
            }
        }
        else return $this->redirect(Yii::$app->homeUrl);
    }


    public function actionFailure()
    {
        Yii::$app->getSession()->setFlash('basketFailed', 'Упс, что-то пошло не так...');
        return $this->redirect(['card/basket']);
    }



}
