<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.12.2019
 * Time: 16:33
 */

namespace frontend\controllers;


use common\models\City;
use common\models\Menu;
use common\models\Review;
use Yii;

class FeedbackController extends FrontendController
{

    public function actionIndex(){
        $model = Menu::findOne(['url'=>'feedback']);
        $this->setMeta($model->metaName,$model->metaKey, $model->metaDesc);
        $city = City::find()->all();
        return $this->render('index',compact('city','model'));
    }



    public function actionAddReview()
    {
        if (Yii::$app->request->isAjax) {
            $model = new Review();
            if ($model->load(Yii::$app->request->post())) {
                $phoneStatus = true;
                for ($i = 0; $i < strlen($model->telephone); $i++) {
                    if ($model->telephone[$i] == '_') {
                        $phoneStatus = false;
                    }
                }
                if ($phoneStatus) {
                    if ($model->save()) {
                        return 1;
                    } else {
                        $review_error = "";
                        $review_errors = $model->getErrors();
                        foreach ($review_errors as $v) {
                            $review_error .= $v[0];
                            break;
                        }
                        return $review_error;
                    }
                } else {
                    return "Необходимо заполнить «Телефон».";
                }

            }
        }else{
            throw new NotFoundHttpException();
        }
    }

}
