<?php

namespace frontend\controllers;

use common\models\UserProfile;
use yii\rest\ActiveController;
use common\models\User;
use yii\httpclient\Client;
use Yii;

class UserController extends ActiveController
{
    public $modelClass = 'frontend\models\Orders';

    public function actionTake()
    {
        $params = [
            "api_key" => "b6753f1549c74fef84a29e42a123585a"
        ];

        $curl = curl_init('https://app.relog.kz/api/v2/applications'. '?' . http_build_query($params). '&statusGroup=new');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);


        $json = curl_exec($curl);

        $json = json_decode($json);

        return $json;
    }



    public function actionTest(){
        $params = [
            "q" => "ищщл",
            "cp" => 4,
            "client" => "psy-ab",
            "xssi" => "t",
            "gs_ri" => "gws-wiz",
            "hl" => "ru-KZ",
            "authuser" => 0,
            "psi" => "6DhMXYypBs3KrgSnyKWQAw.".time(),
            "ei" => "6DhMXYypBs3KrgSnyKWQAw",
        ];

        $curl = curl_init('https://www.google.com/complete/search');
        $data_string = json_encode($params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($curl);
        if ($json) {
            $data = json_decode($json);
            return $data;
        }else{
            echo 'Error';
        }
    }

}